<?php 
/**
 * @file declaration_form.php
 */
$title= "Declarer heures";
require "view_begin.php";
 ?>

<div class="container">
	<div class="row justify-content-center align-items-center">
		<div class="container col-lg-6 col-md-8 col-sm-10 col-12 formulaire">
			<p class="form-titre">Déclarer des heures</p>
				<form id="form" method="post" action="?controller=declaration&action=validation">
					<div class="form-group">
						<label>Heures
							<input id="heures" type="text" class="form-control" size="30" name="heure" required/>
						</label>
					</div>
					<div id="heures-error" class="error-message">
                        <label>
                            <p>Le nombre d'heures n'est pas renseigné ! </p> 
                        </label>
                    </div>
					<div id="heures-error-format" class="error-message">
                        <label>
                            <p>Le nombre d'heures n'est pas valide ! </p> 
                        </label>
                    </div>

					<div class="form-group">
						<label>Année</label>
						<select required="" id="select-annee" name="annee" class="form-select" style="width: 350px;">
							<option value="">Choississez une option</option>
                                <?php foreach($annee as $c): ?>
									<?php foreach($c as $v): ?>
                                 		<option value="<?= e($v) ?>"> <?= e($v) ?> </option>
									<?php endforeach ?>
                                <?php endforeach ?>
						</select>
					</div>
					<div id="select-error-annee" class="error-message">
                        <label>
                            <p>Une année doit être séléctionnée !</p> 
                        </label>
                    </div>

                    <div class="form-group b">Semestre
						<?php foreach($semestre as $c): ?>
							<?php foreach($c as $v): ?>
								<div class="form-check form-g">
									<input required="" class="form-check-input-radio-semestre" type="radio" name="semestre" value="<?= e($v) ?>">
									<label class="form-check-label c"><?= e($v) ?></label>
								</div>
							<?php endforeach ?>
						<?php endforeach ?>
					</div>
					<div id="radio-error-semestre" class="error-message">
                        <label>
                            <p>Un semestre doit être séléctionné !</p> 
                        </label>
                    </div>

					<div class="form-group b">Département
						<div class="form-check form-g d-block">
							<?php foreach($departement as $v): ?>
								<input required="" class="form-check-input-radio-departement" type="radio" name="dept" value=<?= e($v["id"]) ?>>
								<label class="form-check-label c d-block"><?= e($v["nom"]) ?></label>
							<?php endforeach ?>
						</div>
					</div>
					<div id="radio-error-departement" class="error-message">
                        <label>
                            <p>Un département doit être séléctionné !</p> 
                        </label>
                    </div>

					<div class="form-group b">Type d'heure
						<div class="form-check form-g">
							<input required="" class="form-check-input-radio-type-dheures" type="radio" name="type_h" value='statuaire'>
							<label class="form-check-label c">Statuaire</label>
						</div>

						<div class="form-check form-g">
							<input required="" class="form-check-input-radio-type-dheures" type="radio" name="type_h" value='complementaire'>
							<label class="form-check-label c">Complémentaire</label>
						</div>
					</div>
					<div id="radio-error-type-dheures" class="error-message">
                        <label>
                            <p>Un type d'heure doit être séléctionné !</p> 
                        </label>
                    </div>
						<button type="submit" value="Ajouter" class="form-group bouton_v2 ">Ajouter</button>
				</form>
		</div>
	</div>
</div>

<script>

	let form = $('#form');
    let heures = $('#heures');
	let heuresError = $('#heures-error');
	let erHeures = /^[0-9]+$/;
	let heuresErrorFormat = $('#heures-error-format');
	let selectAnnee = $('#select-annee');
	let selectErrorAnnee = $('#select-error-annee');
	let radioSemestre = $('.form-check-input-radio-semestre');
    let radioErrorSemestre = $('#radio-error-semestre');
    let radioErrorCheckSemestre = 0;
	let radioDepartement = $('.form-check-input-radio-departement');
    let radioErrorDepartement = $('#radio-error-departement');
    let radioErrorCheckDepartement = 0;
	let radioTypeHeure = $('.form-check-input-radio-type-dheures');
    let radioErrorTypeHeure = $('#radio-error-type-dheures');
    let radioErrorCheckTypeHeure = 0;


	$(document).ready(function() {

		form.on('submit', function() {
			
			event.preventDefault();

			if (!erHeures.test(heures.val())) {
				alert("Le nombre d'heures n'est pas valide !");
			}
			else {
				alert("Demande de déclaration d'heures réussites !")
        		form.off('submit').submit();
			}


		});


		if (heures.val().length === 0) {
			heuresError.show();
		}
		

		heures.on('keyup', function() {

			if (heures.val().length === 0) {
				heuresError.show();
				heuresErrorFormat.hide();
			}
			else if (!erHeures.test(heures.val())) {
				heuresError.hide();
				heuresErrorFormat.show();
			}
			else {
				heuresError.hide();
				heuresErrorFormat.hide();
			}
		});
	

		if (selectAnnee[0].selectedIndex === 0) {
            selectErrorAnnee.show();
        }

		selectAnnee.on('click', function() {

			if (selectAnnee[0].selectedIndex === 0) {
				selectErrorAnnee.show();
			}
			else {
				selectErrorAnnee.hide();
			}

		});


		for (let i = 0; i < radioSemestre.length; i++) {

			if (!radioSemestre[i].checked) {
				radioErrorCheckSemestre++;
			}
		}

		if (radioErrorCheckSemestre === radioSemestre.length) {
			radioErrorSemestre.show();
		}


		radioSemestre.on('click', function() {

			radioErrorCheckSemestre = 0;

			for (let i = 0; i < radioSemestre.length; i++) {

				if (!radioSemestre[i].checked) {
					radioErrorCheckSemestre++;
				}
			}

			if (radioErrorCheckSemestre !== radioSemestre.length) {
				radioErrorSemestre.hide();
			}

			});



			for (let i = 0; i < radioDepartement.length; i++) {

				if (!radioDepartement[i].checked) {
					radioErrorCheckDepartement++;
				}
			}

			if (radioErrorCheckDepartement === radioDepartement.length) {
				radioErrorDepartement.show();
			}


			radioDepartement.on('click', function() {

				radioErrorCheckDepartement = 0;

				for (let i = 0; i < radioDepartement.length; i++) {

					if (!radioDepartement[i].checked) {
						radioErrorCheckDepartement++;
					}
				}

				if (radioErrorCheckDepartement !== radioDepartement.length) {
					radioErrorDepartement.hide();
				}

			});


			for (let i = 0; i < radioTypeHeure.length; i++) {

				if (!radioTypeHeure[i].checked) {
					radioErrorCheckTypeHeure++;
				}
			}

			if (radioErrorCheckTypeHeure === radioTypeHeure.length) {
				radioErrorTypeHeure.show();
			}


			radioTypeHeure.on('click', function() {

				radioErrorCheckTypeHeure = 0;

				for (let i = 0; i < radioTypeHeure.length; i++) {

					if (!radioTypeHeure[i].checked) {
						radioErrorCheckTypeHeure++;
					}
				}

				if (radioErrorCheckTypeHeure !== radioTypeHeure.length) {
					radioErrorTypeHeure.hide();
				}

			});

	});



</script>

<?php require "view_end.php"; ?>
<?php 

/**
 * @file view_ajouter.php
 * @brief Fichier contenant tous les actions qui agisssent sur la page d'a
 * @details il permet à l'utilisateur de choisir entre ajouter un enseignant ou un secrétaire, en cliquant sur les boutons correspondants

 * @date 24-05-2024 
  */ 
$title= "Ajouter un utilisateur";
require "view_begin.php";
?>

    <div id="cont_case" class="container">
        <h1 class="h1titre">Quel type d'utilisateur <br> voulez-vous ajouter ?</h1>
        <a href="?controller=annuaire&action=ajouter_form&poste=enseignant">
            <button class="ajout bouton_v2">Enseignant</button>
        </a>
        <a href="?controller=annuaire&action=ajouter_form&poste=secretaire">
            <button class="ajout bouton_v2">Secrétaire</button>
        </a>
    </div>

<?php require "view_end.php"; ?>
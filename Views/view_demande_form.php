<?php
/**
 * @file view_demande_form.php
 */
$title= "Formulaire de demande";
require "view_begin.php"; 
 ?>
<div class="container">
	<div class="row justify-content-center align-items-center">
		<div class="container col-lg-6 col-md-8 col-sm-10 col-12 formulaire">
			<p class="form-titre">Formulaire de demande</p>
                <form id="form" method="post" action="?controller=departement&action=validation">
					<div class="form-group">
						<label>Heures
							<input id ="heures" type="text" class="form-control" size="30" name="besoin" required/>
						</label>
                    </div>
					<div id="heures-error" class="error-message">
                        <label>
                            <p>Le nombre d'heures n'est pas renseigné ! </p> 
                        </label>
                    </div>
					<div id="heures-error-format" class="error-message">
                        <label>
                            <p>Le nombre d'heures n'est pas valide ! </p> 
                        </label>
                    </div>

					<div class="form-group">
						<label>Année</label>
							<select required= "" id="select-annee" name="annee" class="form-select" style="width: 350px;" >
								<option value="">Choississez l'année</option>
                                <?php foreach($annee as $c): ?>
									<?php foreach($c as $v): ?>
                                 		<option value="<?= e($v) ?>"> <?= e($v) ?> </option>
									<?php endforeach ?>
                                <?php endforeach ?>
							</select>
					</div>
					<div id="select-error-annee" class="error-message">
                        <label>
                            <p>Une année doit être séléctionnée !</p> 
                        </label>
                    </div>

                    <div class="form-group b">Semestre
						<?php foreach($semestre as $c): ?>
							<?php foreach($c as $v): ?>
							<div class="form-check form-g">
								<input required= "" class="form-check-input-radio-semestre" type="radio" name="semestre" value="<?= e($v) ?>">
								<label class="form-check-label c"><?= e($v) ?></label>
							</div>
							<?php endforeach ?>
						<?php endforeach ?>
					</div>
					<div id="radio-error-semestre" class="error-message">
                        <label>
                            <p>Un semestre doit être séléctionné !</p> 
                        </label>
                    </div>

					<div class="form-group b">Formation
						<div class="form-check form-g d-block">
							<?php foreach($formation as $v): ?>
								<input required= "" class="form-check-input-radio-formation" type="radio" name="formation" value=<?= e($v["id"]) ?>>
								<label class="form-check-label c d-block"><?= e($v["nom"]) ?></label>
							<?php endforeach ?>
						</div>
					</div>

					<div id="radio-error-formation" class="error-message">
                        <label>
                            <p>Une formation doit être séléctionnée !</p> 
                        </label>
                    </div>
                        
					<div class="form-group b">Discipline

							<select required="" id="select-discipline" name="discipline" class="form-select" style="width: 350px;">
								<option value="">Choississez la discipline</option>
                                	<?php foreach($discipline as $v): ?>
									   <option value="<?= e($v["id"]) ?>"> <?=e($v["nom"]) ?> </option>
                                    <?php endforeach ?>
							</select>
										
					</div>
					<div id="select-error-discipline" class="error-message">
                        <label>
                            <p>Une discipline doit être séléctionnée !</p> 
                        </label>
                    </div>

					<button type="submit" value="Envoyer" class="form-group bouton_v2 ">Envoyer la demande</button>
				</form>
		</div>
	</div>
</div>

<script>


    let form = $('#form');
    let heures = $('#heures');
	let heuresError = $('#heures-error');
	let erHeures = /^[0-9]+$/;
	let heuresErrorFormat = $('#heures-error-format');
	let selectAnnee = $('#select-annee');
	let selectErrorAnnee = $('#select-error-annee');
	let radioSemestre = $('.form-check-input-radio-semestre');
    let radioErrorSemestre = $('#radio-error-semestre');
    let radioErrorCheckSemestre = 0;
	let radioFormation = $('.form-check-input-radio-formation');
    let radioErrorFormation = $('#radio-error-formation');
    let radioErrorCheckFormation = 0;
	let selectDiscipline = $('#select-discipline');
	let selectErrorDiscipline = $('#select-error-discipline');




	$(document).ready(function() {


		form.on('submit', function() {
			
			event.preventDefault();

			if (!erHeures.test(heures.val())) {
				alert("Le nombre d'heures n'est pas valide !");
			}
			else {
				alert("Demande de déclaration d'heures réussites !")
        		form.off('submit').submit();
			}


		});


		if (heures.val().length === 0) {
			heuresError.show();
		}
		

		heures.on('keyup', function() {

			if (heures.val().length === 0) {
				heuresError.show();
				heuresErrorFormat.hide();
			}
			else if (!erHeures.test(heures.val())) {
				heuresError.hide();
				heuresErrorFormat.show();
			}
			else {
				heuresError.hide();
				heuresErrorFormat.hide();
			}
		});

		if (selectAnnee[0].selectedIndex === 0) {
            selectErrorAnnee.show();
        }

		selectAnnee.on('click', function() {

			if (selectAnnee[0].selectedIndex === 0) {
				selectErrorAnnee.show();
			}
			else {
				selectErrorAnnee.hide();
			}

		});

		for (let i = 0; i < radioSemestre.length; i++) {

			if (!radioSemestre[i].checked) {
				radioErrorCheckSemestre++;
			}
			}

		if (radioErrorCheckSemestre === radioSemestre.length) {
			radioErrorSemestre.show();
		}


		radioSemestre.on('click', function() {

			radioErrorCheckSemestre = 0;

			for (let i = 0; i < radioSemestre.length; i++) {

				if (!radioSemestre[i].checked) {
					radioErrorCheckSemestre++;
				}
			}

			if (radioErrorCheckSemestre !== radioSemestre.length) {
					radioErrorSemestre.hide();
			}

		});


		for (let i = 0; i < radioFormation.length; i++) {

			if (!radioFormation[i].checked) {
				radioErrorCheckFormation++;
			}
		}

		if (radioErrorCheckFormation === radioFormation.length) {
			radioErrorFormation.show();
		}


		radioFormation.on('click', function() {

			radioErrorCheckFormation = 0;

			for (let i = 0; i < radioFormation.length; i++) {

				if (!radioFormation[i].checked) {
					radioErrorCheckFormation++;
				}
			}

			if (radioErrorCheckFormation !== radioFormation.length) {
				radioErrorFormation.hide();
			}

		});


		if (selectDiscipline[0].selectedIndex === 0) {
            selectErrorDiscipline.show();
        }

		selectDiscipline.on('click', function() {

			if (selectDiscipline[0].selectedIndex === 0) {
				selectErrorDiscipline.show();
			}
			else {
				selectErrorDiscipline.hide();
			}

		});



	});

</script>


<?php require "view_end.php"; ?>
<?php 
$title= "Annuaire";
require "view_begin.php"; 
?>

<form>

    <p>
        <input type="hidden" name="controller" value="annuaire"/>
    </p>

    <p>
        <input type="hidden" name="action" value="annuaire"/>
    </p>

    <p class="ajt">
        <label>
            <input class='resr' type="text" name="recherche" placeholder='Rechercher...'/>  
            <input class='bouton_v3' type="submit" value="Rechercher"/>
        </label>
    </p>    
</form>

<!-- Condition : Afficher le bouton "Ajouter" uniquement si l'utilisateur a la permission "direction" -->
<?php if(isset($_SESSION["permission"]) and $_SESSION["permission"]=="direction"):?>
    <p class='ajt'>
        <label>
            <a href="?controller=annuaire&action=ajouter">
                <button class='bouton_v3'>Ajouter</button>
            </a>
        </label>
    </p>
<?php endif ?>

<?php if (!$infos) : ?>
    <?php echo "<div style='display : flex;align-items: center;justify-content: center;'>" ?>
        <?php echo "<h1 style='color : white';> Aucune personne ne correspond ! </h1>" ?>
    <?php echo "</div>" ?>
<?php endif ?>


<div id="supprimer-confirm-div" class="confirm-message" style="display: none;">
    <label>
        <p id="confirm-message-text"></p>
        <button id="oui" type="button"> Oui </button>
        <button id="non" type="button"> Non </button>
    </label>
</div>


<div id='cont_case' class='container-fluid col justify-content-center align-items-center'>
    <div class='row marge'>
        <table>
            
            <tr class='texttr' class='row'>
                <td>
                    <strong>Nom</strong>
                </td>
                <td>
                    <strong>Prénom</strong>
                </td>
                <td>
                    <strong>Grade</strong>
                </td>
                <!-- Condition : Ajouter une colonne pour supprimer si l'utilisateur a la permission "direction" -->
                <?php if(isset($_SESSION["permission"]) and $_SESSION["permission"]=="direction"):?>     
                    <td>
                        <strong>Actions</strong>
                    </td>
                <?php endif ?>
            </tr>
            
            <?php foreach($infos as $v): ?>
                <tr>
                    <td>
                        
                        <a class="lien" href="?controller=profil&action=profil&id=<?= e($v["id_personne"])?>"><?=e($v["nom"])?></a>
                    </td>
                    <td>
                        
                        <a class="lien" href="?controller=profil&action=profil&id=<?= e($v["id_personne"])?>"><?=e($v["prenom"])?></a>
                    </td>
                    <td class="fonction">
                        
                        <?= e($v["fonction"])?>
                    </td>
                    <?php if(isset($_SESSION["permission"]) and $_SESSION["permission"]=="direction"):?>
                        <?php if ($_SESSION['id'] != $v['id_personne']): ?>
                            <td>
                                <a href="javascript:void(0);" class="delete-link" data-id="<?= e($v["id_personne"])?>" data-nom="<?= e($v["nom"])?>" data-prenom="<?= e($v["prenom"])?>" data-fonction="<?= e($v["fonction"])?>">
                                    <img id="supprimer" class="croix" src="Content/img/icons8-cross-in-circle-100.png" alt="supprimer"/>
                                </a>
                            </td>
                        <?php endif ?>

                    <?php endif ?>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
</div>
<script>
    const link = document.getElementById('link');
     // Ajouter un event listener au lien de suppression
        link.addEventListener('click', function(event) {
                event.preventDefault();
                const userConfirmation = confirm("Êtes-vous sûr de vouloir supprimer cet élément ?");
                if (userConfirmation) {
                    // Redirige vers l'URL de suppression
                    window.location.href = this.link;
                }
            });
    </script>

<script>

    let supprimer = $('#supprimer');
    let confirmDiv = $('#supprimer-confirm-div');
    currentDeleteLink = null;


    
    $(document).ready(function() {

        $('.delete-link').on('click', function() {
            currentDeleteLink = $(this);
            let nom = currentDeleteLink.data('nom');
            let prenom = currentDeleteLink.data('prenom');
            let fonction = currentDeleteLink.data('fonction');
            $('#confirm-message-text').text(`Voulez-vous vraiment supprimer ${prenom} ${nom}, ${fonction} ?`);

            confirmDiv.show();
        });

        $('#oui').on('click', function() {
            if (currentDeleteLink) {
                window.location.href = "?controller=annuaire&action=supprimer&id=" + currentDeleteLink.data('id');
            }
        });

        $('#non').on('click', function() {

            confirmDiv.hide();
        });
    });

</script>


<?php require "view_end.php"; ?>
<?php
/**
 * @file view_assistance.php
 * @brief ce fichier affiche le contenu du module assistance
 */
/** @var string $title */
$title= "Assistance";
require "view_begin.php";
?>

<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="container col-lg-6 col-md-8 col-sm-10 col-12 formulaire">
            <p class="form-titre">Assistance</p>
            <form action="?controller=assistance&action=send_email" method="post">

                <div class="form-group">
                    <label>Nom<input type="text" class="form-control" size="25" value="<?= e($profil['nom'])?>" readonly></label>
                    <label>Prénom<input type="text" class="form-control" size="25" value="<?= e($profil['prenom'])?>" readonly></label>
                </div>

                <div class="form-group">
                    <label>Identifiant<input type="text" name="id" class="form-control" size="30" value="<?= e($profil['id'])?>" readonly></label>
                </div>

                <div class="form-group">
                    <label>Mail<input type="email" name="email" class="form-control" size="30" value="<?= e($profil['email'])?>" readonly></label>
                </div>

                <div class="form-group">
                    <label>Objet<input id="objet" name="subject" type="text" class="form-control" size="30" required></label>
                </div>

                <div id="objet-error" class="error-message">
                    <label>
                        <p>L'objet n'est pas renseigné !</p> 
                    </label>
                </div>

                <div class="form-group">
                    <label>Demande<textarea id="demande" name="message" class="form-control" rows="5" cols="40" required></textarea></label>
                </div>

                <div id="demande-error" class="error-message">
                    <label>
                        <p>La demande n'est pas renseignée !</p> 
                    </label>
                </div>

                <button type="submit" class="form-group bouton_v2 ">Envoyer</button>

            </form>

        </div>
    </div>
</div>

	<script>

		let objet = $('#objet');
		let objetError = $('#objet-error');

		let demande = $('#demande');
		let demandeError = $('#demande-error');


		$(document).ready(function() {

			if (objet.val().length === 0) {
                objetError.show();
        	}
			else {
				objetError.hide();
			}

			objet.on('keyup', function() {

				if (objet.val().length !== 0) {
					objetError.hide();
				}
				else {
					objetError.show();
				}

			});


			if (demande.val().length === 0) {
                demandeError.show();
        	}
			else {
				demandeError.hide();
			}

			demande.on('keyup', function() {

				if (demande.val().length !== 0) {
					demandeError.hide();
				}
				else {
					demandeError.show();
				}

			});



		});



	</script>


<?php require "view_end.php" ?>
<?php 
/** @view_modification.php */
$title= "Modifier son profil";
require "view_begin.php"; 
?>

<div class="container">
	<div class="row justify-content-center align-items-center">
		<div class="container col-lg-6 col-md-8 col-sm-10 col-12 formulaire">
			<p class="form-titre">Modifier profil</p>
                <form id="form" method="post" action="?controller=profil&action=modifier">
                    <div class="form-group">

                        <label>
                            <input type="hidden" class="form-control" size="30" name="fonction" value="<?= e($profil['fonction'])?>"/>
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="hidden" class="form-control" size="30" name="id" value="<?= e($profil['id'])?>"/>
                        </label>
                    </div>
					<div class="form-group">
                        <label>Nom
                            <input id="name" required= "" type="text" class="form-control" size="30" name="nom" value="<?= e($profil['nom'])?>"/>
                        </label>
                    </div>
                    <div id="name-error" class="error-message">
                        <label>
                            <p>Le nom n'est pas renseigné !</p> 
                        </label>
                    </div>
					<div class="form-group">
                        <label>Prénom
                            <input id="surname" required= "" type="text" class="form-control" size="30" name="prenom" value="<?= e($profil['prenom'])?>"/>
                        </label>
                    </div>
                    <div id="surname-error" class="error-message">
                        <label>
                            <p>Le prénom n'est pas renseigné !</p> 
                        </label>
                    </div>
					<div class="form-group">
                        <label>Email
                            <input id="email" required= "" type="text" class="form-control" size="30" name="email" placeholder="exemple@gmail.com" value="<?= e($profil['email'])?>"/>
                        </label>
                    </div>
                    <div id="email-error" class="error-message">
                        <label>
                            <p>L'email n'est pas renseigné !</p> 
                        </label>
                    </div>
                    <div id="email-error-format" class="error-message">
                        <label>
                            <p>L'email n'est pas valide !</p> 
                        </label>
                    </div>
					<div class="form-group">
                        <label>Téléphone
                            <input id="phone" required= "" type="text" class="form-control" size="30" name="phone" placeholder="0123456789" value="<?= e($profil['phone'])?>"/>
                        </label>
                    </div>
                    <div id="phone-error" class="error-message">
                        <label>
                            <p>Le numéro de téléphone n'est pas renseigné !</p> 
                        </label>
                    </div>
                    <div id="phone-error-format" class="error-message">
                        <label>
                            <p>Le format du numéro de téléphone n'est pas respecté !</p> 
                        </label>
                    </div>
					<div class="form-group">
                        <label>Mot de passe
                            <input id="password" required= "" type="password" class="form-control" size="30" name="mdp" />
                        </label>
                    </div>
                    <div id="password-error" class="error-message">
                        <label>
                            <p>Le mot de passe n'est pas renseigné !</p> 
                        </label>
                    </div>

                    <!-- Vérifie si la fonction du profil n'est ni "secretaire" ni "personne" -->

                    <?php if($profil["fonction"] != "secretaire" and $profil["fonction"] != "personne") :?>
                        <div class="form-group">
							<label>Année</label>
							    <select id="select-annee" required="" name="annee" class="form-select" style="width: 350px;">
								    <option value="">Choississez une option</option>
                                    <?php foreach($annee as $c): ?>
									    <?php foreach($c as $v): ?>
                                 		    <option value="<?= e($v) ?>"> <?= e($v) ?> </option>
									    <?php endforeach ?>
                                    <?php endforeach ?>
							    </select>
						</div>
                        <div id="select-error" class="error-message">
                            <label>
                                <p>Une année doit être séléctionnée !</p> 
                            </label>
                        </div>

                        <div class="form-group b">Semestre
						    <?php foreach($semestre as $c): ?>
							    <?php foreach($c as $v): ?>
							        <div class="form-check form-g">
								    <input required= "" class="form-check-input-radio" type="radio" name="semestre" value="<?= e($v) ?>">
								    <label class="form-check-label c"><?= e($v) ?></label>
						</div>
							    <?php endforeach ?>
						    <?php endforeach ?>
						</div>
                        <div id="radio-error" class="error-message">
                            <label>
                                <p>Au moins une des cases doit être cochée !</p> 
                            </label>
                        </div>

                        <div class="form-group">
							<label>Statut</label>
							    <select required= "" name="statut" class="form-select" style="width: 350px;">
                                    <?php $dis = true; ?>

                                        <!-- Effectue un tour de boucle grâce à la variable $dis, au lieu de parcourir $list['status'] plusieurs fois pour rien -->

                                        <?php foreach($list['statut'] as $v): ?>
                                            <?php if ($dis): ?>
                                                <option value="<?= e($profil["statut"]) ?>"> <?= e($profil["statut"]) ?> </option>
                                            <?php $dis = false; ?>
                                        <?php endif ?>
                                        <!-- Cette condition vérifie si le sigle de catégorie actuel est différent du statut du profil.
                                        Si c'est le cas, une option de sélection est générée avec la valeur du sigle de catégorie. -->
                                        <?php if ($v["siglecat"] != $profil["statut"]):?>
                                            <option value="<?= e($v["siglecat"]) ?>"> <?= e($v["siglecat"]) ?> </option>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </select>
						</div>

                        <div class="form-group">
							<label>Discipline</label>
							    <select required= "" name="discipline" class="form-select" style="width: 350px;;">
                                    <?php $dis = true; ?>

                                        <!-- Effectue un seul tour de boucle grâce à la variable $dis, au lieu de parcourir $list['discipline'] plusieurs fois pour rien -->

                                        <?php foreach($list['discipline'] as $v): ?>
                                            <?php if ($dis): ?>
                                                <option value="<?= e($profil["discipline"]) ?>"> <?= e($profil["discipline"]) ?> </option>
                                                <?php $dis = false; ?>
                                            <?php endif ?>
                                            <!-- Cette condition vérifie si le libellé de la discipline actuelle est différent de la discipline du profil.
                                            Si c'est le cas, une option de sélection est générée avec la valeur du libellé de la discipline. -->

                                            <?php if ($v["libelledisc"] != $profil["discipline"]):?>
                                                <option value="<?= e($v["libelledisc"]) ?>"> <?= e($v["libelledisc"]) ?> </option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                </select>
                        </div>

                        <div class="form-group b">Départements</br>
                            <?php foreach ($list['departements'] as $v): ?>
                                <?php $deptFound = false; ?>
                                    <?php foreach ($profil['depts'] as $dept) : ?>
                                        <!-- Cette condition vérifie si le libellé du département actuel est égal au libellé du département dans la liste des départements associés au profil.
                                        Si c'est le cas, un département est sélectionné. -->

                                        <?php if ($v['libelledept'] == $dept['depts']) : ?>
                                            <div class="form-check form-g">
								                <input class="form-check-input-checkbox" type="checkbox" name="departements[]" value="<?= e($dept['depts'])?>" checked/>
                                                <label class="form-check-label c"><?= e($dept['depts']) ?></label></br>
                                                <?php $deptFound = true; ?>
							                </div>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                    <!-- Cette condition vérifie si aucun département n'a été trouvé dans la liste des départements associés au profil.
                                    Si c'est le cas, aucun département n'est sélectionné. -->

                                    <?php if (!$deptFound): ?>
                                        <input class="form-check-input-checkbox" type="checkbox" name="departements[]" value="<?= e($v["libelledept"])?>"/>
                                        <label class="form-check-label c"><?= e($v["libelledept"]) ?></label></br>
                                        <?php if (e($v["libelledept"]) == '') {
                                            echo "erreur !";
                                        }
                                        
                                        
                                        ?>
                                    <?php endif ?>
                                <?php endforeach ?>
                        </div>

                    <?php endif ?>

                    <div id="checkbox-error" class="error-message">
                        <label>
                            <p>Au moins une des cases doit être cochée !</p> 
                        </label>
                    </div>


                        <button type="submit" value="Mettre à jour" class="form-group bouton_v2 ">Mettre à jour</button>

                </form>
        </div>
	</div>
</div>


<script>
    
    let name = $('#name');
    let nameError = $('#name-error');
    let surname = $('#surname');
    let surnameError = $('#surname-error');
    let email = $('#email');
    let emailError = $('#email-error');
    let emailErrorFormat = $('#email-error-format');
    let erEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    let phone = $('#phone');
    let phoneError = $('#phone-error');
    let phoneErrorFormat = $('#phone-error-format');
    let erPhone = /^[0-9]{10}$/;
    let password = $('#password');
    let passwordError = $('#password-error');
    let checkbox = $('.form-check-input-checkbox');
    let checkboxError = $('#checkbox-error');
    let checkboxErrorCheck = 0;
    let form = $('#form');
    let radio = $('.form-check-input-radio');
    let radioError = $('#radio-error');
    let radioErrorCheck = 0;
    let selectAnnee = $('#select-annee');
    let selectError = $('#select-error');

   
    $(document).ready(function() {

        form.on('submit', function() {

            checkboxErrorCheck = 0;

            event.preventDefault();

        if (checkbox.length !== 0) {
            for (let i = 0; i < checkbox.length; i++) {

                if (!checkbox[i].checked) {
                    checkboxErrorCheck++;
    
                }
            }

            if (!erEmail.test(email.val())) {
                alert("Le format de l'email n'est pas respecté ! ");
            }

            else if (checkboxErrorCheck === checkbox.length) {
                alert('Veuillez cocher au moins une case des départements !');
                event.preventDefault();
            }

            else if (!erPhone.test(phone.val())) {
                alert("Le format du numéro de téléphone n'est pas respecté ! ");
            }
            else {
                alert('Modifications réussites !')
                form.off('submit').submit();
            }

        }
        else {
            if (!erEmail.test(email.val())) {
                alert("Le format de l'email n'est pas respecté ! ");
            }

            else if (!erPhone.test(phone.val())) {
                alert("Le format du numéro de téléphone n'est pas respecté ! ");
            }

            else {
                alert('Modification réussites !')
                form.off('submit').submit();
            }
        }



        });



        name.on('keyup', function() {

            if (name.val().length === 0) {
            
                nameError.show();
            }
            else {
            
                nameError.hide();
            }

        });

        surname.on('keyup', function() {

            if (surname.val().length === 0) {
           
                surnameError.show();
            }
            else {
            
                surnameError.hide();
            }

        });

        email.on('keyup', function() {

            if (email.val().length === 0) {
                emailError.show();
                emailErrorFormat.hide(); 
            } else if (!erEmail.test(email.val())) {
                emailErrorFormat.show();
                emailError.hide(); 
            } else {
                emailError.hide();
                emailErrorFormat.hide();
            }

        });

        phone.on('keyup', function() {

            if (phone.val().length === 0) {
                phoneError.show();
                phoneErrorFormat.hide(); 
            } else if (!erPhone.test(phone.val())) {
                phoneErrorFormat.show();
                phoneError.hide(); 
            } else {
                phoneError.hide();
                phoneErrorFormat.hide();
            }

        });

        if (password.val().length === 0) {
                passwordError.show();
        }

        password.on('keyup', function() {

            if (password.val().length === 0) {
                passwordError.show();
            }
            else {
                passwordError.hide();
            }
        
        });

        if (selectAnnee[0].selectedIndex === 0) {
            selectError.show();
        }

        selectAnnee.on('click', function() {

            if (selectAnnee[0].selectedIndex === 0) {
                selectError.show();
            }
            else {
                selectError.hide();
            }

        });

        for (let i = 0; i < radio.length; i++) {

            if (!radio[i].checked) {
                radioErrorCheck++;
            }
        }

        if (radioErrorCheck === radio.length) {
            radioError.show();
        }
    

        radio.on('click', function() {

            radioErrorCheck = 0;

            for (let i = 0; i < radio.length; i++) {

                if (!radio[i].checked) {
                    radioErrorCheck++;
                }
            }

            if (radioErrorCheck !== radio.length) {
                radioError.hide();
            }

        });

        if (checkbox.length !== 0) {
            for (let i = 0; i < checkbox.length; i++) {

                if (!checkbox[i].checked) {
                    checkboxErrorCheck++;
                }
            }

            if (checkboxErrorCheck === checkbox.length) {
                checkboxError.show();
            }



            checkbox.on('click', function() {

                let checkboxErrorCheck = 0;

                for (let i = 0; i < checkbox.length; i++) {

                    if (!checkbox[i].checked) {
                        checkboxErrorCheck++;
                    }
                }

                if (checkboxErrorCheck === checkbox.length) {
                    alert('Vous devez cocher au moins une case !');
                    event.preventDefault();
                }
                else {
                    checkboxError.hide();
                }

            });

        }
        
    });



</script>


<?php require "view_end.php" ?>
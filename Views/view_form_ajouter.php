<?php 
/**
 * @file view_form_departement.php
 */
$title= "Ajouter un utilisateur";
require "view_begin.php"; 
 ?>

<div class="container">
	<div class="row justify-content-center align-items-center">
		<div class="container col-lg-6 col-md-8 col-sm-10 col-12 formulaire">
			<p class="form-titre">Ajouter un utilisateur</p>
				<form id="form" method="post" action="?controller=annuaire&action=validation">
                    <input type="hidden" value="<?php if(isset($_GET["poste"])){echo e($_GET["poste"]);} ?>" name="poste"/>
						<div class="form-group"><label>ID Interne<input id="id" type="text" class="form-control" size="30" name="id" required/></label></div>
						<div id="id-error" class="error-message">
							<label>
								<p>L'ID n'est pas renseigné !</p> 
							</label>
                   		 </div>
						<div id="id-error-format" class="error-message">
							<label>
								<p>L'ID n'est pas valide !</p> 
							</label>
                    	</div>
						<div id="id-error-exists" class="error-message">
							<label>
								<p>L'ID existe déjà dans la base de données !</p> 
							</label>
						</div>
						<div class="form-group"><label>MDP<input id="password" type="text" class="form-control" size="30" name="mdp" required/></label></div>
						<div id="password-error" class="error-message">
							<label>
								<p>Le mot de passe n'est pas renseigné !</p> 
							</label>
                   		 </div>
						<div class="form-group"><label>Nom<input id="name" type="text" class="form-control" size="30" name="nom" required/></label></div>
						<div id="name-error" class="error-message">
							<label>
								<p>Le nom n'est pas renseigné !</p> 
							</label>
                   		 </div>
						<div class="form-group"><label>Prénom<input id="surname" type="text" class="form-control" size="30" name="prenom" required/></label></div>
						<div id="surname-error" class="error-message">
							<label>
								<p>Le prénom n'est pas renseigné !</p> 
							</label>
                   		 </div>
						<div class="form-group"><label>Email<input id="email" type="text" class="form-control" size="30" name="email" required/></label></div>
						<div id="email-error" class="error-message">
							<label>
								<p>L'email n'est pas renseigné !</p> 
							</label>
                   		 </div>
						<div id="email-error-format" class="error-message">
							<label>
								<p>L'email n'est pas valide !</p> 
							</label>
                    	</div>
						<div id="email-error-exists" class="error-message">
							<label>
								<p>L'email existe déjà dans la base de données !</p> 
							</label>
						</div>
						<div class="form-group"><label>Téléphone<input id="phone" type="text" class="form-control" size="30" name="phone" required/></label></div>
						<div id="phone-error" class="error-message">
							<label>
								<p>Le numéro de téléphone n'est pas renseigné !</p> 
							</label>
                   		 </div>
						<div id="phone-error-format" class="error-message">
							<label>
								<p>Le numéro de téléphone n'est pas valide !</p> 
							</label>
                    	</div>
						<div id="phone-error-exists" class="error-message">
							<label>
								<p>Le numéro de téléphone existe déjà dans la base de données !</p> 
							</label>
						</div>
                        <?php if(isset($_GET["poste"]) and $_GET["poste"] == "enseignant"):?>

						<div class="form-group">
							<label>Année</label>
							<select id="select-annee" required="" name="annee" class="form-select" style="width: 350px;">
								<option value="">Choississez une option</option>
                                <?php foreach($annee as $c): ?>
									<?php foreach($c as $v): ?>
                                 		<option value="<?= e($v) ?>"> <?= e($v) ?> </option>
									<?php endforeach ?>
                                <?php endforeach ?>
							</select>
						</div>
						<div id="select-error-annee" class="error-message">
							<label>
								<p>Une année doit être séléctionnée !</p> 
							</label>
                    	</div>

						<div class="form-group b">Semestre
							<?php foreach($semestre as $c): ?>
								<?php foreach($c as $v): ?>
									<div class="form-check form-g">
										<input  required= "" class="form-check-input-radio-semestre" type="radio" name="semestre" value="<?= e($v) ?>">
										<label class="form-check-label c"><?= e($v) ?></label>
									</div>
								<?php endforeach ?>
							<?php endforeach ?>
						</div>
						<div id="radio-error-semestre" class="error-message">
							<label>
								<p>Un semestre doit être séléctionné !</p> 
							</label>
                    	</div>

						<div class="form-group">
							<label>Statut</label>
							<select id="select-statut" required="" name="statut" class="form-select" style="width: 350px;;">
								<option value=''>Choississez une option</option>
                                <?php foreach($list["statut"] as $v): ?>
                                    <option value="<?= e($v["siglecat"]) ?>"> <?= e($v["siglecat"]) ?> </option>
                                <?php endforeach ?>
						  	</select>
						</div>
						<div id="select-error-statut" class="error-message">
							<label>
								<p>Un statut doit être séléctionné !</p> 
							</label>
                    	</div>

						<div class="form-group">
							<label>Discipline</label>
							<select id="select-discipline" required="" name="discipline" class="form-select" style="width: 350px;;">
								<option value=''>Choississez une option</option>
                            		<?php foreach($list['discipline'] as $v): ?>
                                		<option value="<?= e($v["libelledisc"]) ?>"> <?= e($v["libelledisc"]) ?> </option>
                            		<?php endforeach ?>
						  	</select>
						</div>
						<div id="select-error-discipline" class="error-message">
							<label>
								<p>Une discipline doit être séléctionnée !</p> 
							</label>
                    	</div>


						<div class="form-group b">Départements</br>
                        	<?php foreach($list['departements'] as $v): ?>
								<div class="form-check form-g">
									<input class="form-check-input-checkbox" type="checkbox" name="departements[]" value="<?= e($v["libelledept"]) ?>"/>
									<label class="form-check-label c"><?= e($v["libelledept"]) ?></label>
								</div>
                        	<?php endforeach ?>

						<div id="checkbox-error" class="error-message">
							<label>
								<p>Au moins une des cases doit être cochée !</p> 
							</label>
                    	</div>

						<div class="form-group b">Membre de direction
							<div class="form-check form-g">
								<input required="" class="form-check-input-radio-direction" type="radio" name="direction" value="true">
								<label class="form-check-label">Oui</label>
							</div>
							<div class="form-check form-g">
								<input required="" class="form-check-input-radio-direction" type="radio" name="direction" value="true">
								<label class="form-check-label">Non</label>
							</div>
						</div>
						<div id="radio-error-direction" class="error-message">
							<label>
								<p>Une option doit être séléctionné !</p> 
							</label>
                    	</div>

                        <?php endif ?>
						<button type="submit" value="Ajouter" class="form-group bouton_v2 ">Ajouter</button>
				</form>
		</div>
	</div>
</div>

<script>

	let form = $('#form');
	let id = $('#id');
    let idError = $('#id-error');
    let idErrorFormat = $('#id-error-format');
    let erId = /^[0-9]+$/;
	let idErrorExists = $('#id-error-exists');
	let password = $('#password');
    let passwordError = $('#password-error');
	let name = $('#name');
    let nameError = $('#name-error');
    let surname = $('#surname');
    let surnameError = $('#surname-error');
	let email = $('#email');
	let emailError = $('#email-error');
    let emailErrorFormat = $('#email-error-format');
    let erEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
	let emailErrorExists = $('#email-error-exists');
	let phone = $('#phone');
	let phoneError = $('#phone-error');
    let phoneErrorFormat = $('#phone-error-format');
    let erPhone = /^[0-9]{10}$/;
	let phoneErrorExists = $('#phone-error-exists');
	let selectAnnee = $('#select-annee');
	let selectErrorAnnee = $('#select-error-annee');
	let radioSemestre = $('.form-check-input-radio-semestre');
    let radioErrorSemestre = $('#radio-error-semestre');
    let radioErrorCheckSemestre = 0;
	let selectStatut = $('#select-statut');
	let selectErrorStatut = $('#select-error-statut');
	let selectDiscipline = $('#select-discipline');
	let selectErrorDiscipline = $('#select-error-discipline');
	let checkbox = $('.form-check-input-checkbox');
    let checkboxError = $('#checkbox-error');
    let checkboxErrorCheck = 0;
	let radioDirection = $('.form-check-input-radio-direction');
    let radioErrorDirection = $('#radio-error-direction');
    let radioErrorCheckDirection = 0;



	$(document).ready(function() {

        form.on('submit', function() {

			checkboxErrorCheck = 0;

			event.preventDefault();

			if (checkbox.length !== 0) {
				for (let i = 0; i < checkbox.length; i++) {

					if (!checkbox[i].checked) {
						checkboxErrorCheck++;
		
					}
				}

				if (!erId.test(id.val())) {
					alert("Le format de l'id n'est pas respecté ! ");
				}

				else if (!erEmail.test(email.val())) {
					alert("Le format de l'email n'est pas respecté ! ");
				}

				else if (!erPhone.test(phone.val())) {
					alert("Le format du numéro de téléphone n'est pas respecté ! ");
				}
				else if (checkboxErrorCheck === checkbox.length) {
                	alert('Veuillez cocher au moins une case des départements !');
                	event.preventDefault();
            	}

				else {
					$.post("?controller=annuaire&action=check_id", { id: id.val() }, function(id) {
						if (id.exists) {
							alert("L'ID existe déjà dans la base de données !");
						} else {

							$.post("?controller=annuaire&action=check_email", { email: email.val() }, function(email) {
								if (email.exists) {
									alert("L'email existe déjà dans la base de données !");
								} else {
									$.post("?controller=annuaire&action=check_phone", { phone: phone.val() }, function(phone) {
										if (phone.exists) {
											alert("Le numéro de téléphone existe déjà dans la base de données !");
										} else {
											alert('Ajout réussi !');
											form.off('submit').submit();
										}
									}, "json");
								}
							}, "json");

						}
					}, "json");

				}
			}
			else {
				if (!erId.test(id.val())) {
					alert("Le format de l'id n'est pas respecté ! ");
				}

				else if (!erEmail.test(email.val())) {
					alert("Le format de l'email n'est pas respecté ! ");
				}

				else if (!erPhone.test(phone.val())) {
					alert("Le format du numéro de téléphone n'est pas respecté ! ");
				}

				else {
					$.post("?controller=annuaire&action=check_id", { id: id.val() }, function(id) {
						if (id.exists) {
							alert("L'ID existe déjà dans la base de données !");
						} else {

							$.post("?controller=annuaire&action=check_email", { email: email.val() }, function(email) {
								if (email.exists) {
									alert("L'email existe déjà dans la base de données !");
								} else {
									$.post("?controller=annuaire&action=check_phone", { phone: phone.val() }, function(phone) {
										if (phone.exists) {
											alert("Le numéro de téléphone existe déjà dans la base de données !");
										} else {
											alert('Ajout réussi !');
											form.off('submit').submit();
										}
									}, "json");
								}
							}, "json");

						}
					}, "json");

				}
			}

		});



		if (id.val().length === 0) {
			idError.show();
        }

		id.on('keyup', function() {

			if (id.val().length === 0) {
				idError.show();
				idErrorFormat.hide();
				idErrorExists.hide();

			} else if (!erId.test(id.val())) {
				idErrorFormat.show();
				idError.hide(); 
			} else {
				idError.hide();
				idErrorFormat.hide();
				$.post("?controller=annuaire&action=check_id", { id: id.val() }, function(id) {
                    if (id.exists) {
                        idErrorExists.show();
                    } else {
                        idErrorExists.hide();
                    }
                }, "json");
			}

		});

		if (password.val().length === 0) {
                passwordError.show();
        }

        password.on('keyup', function() {

            if (password.val().length === 0) {
                passwordError.show();
            }
            else {
                passwordError.hide();
            }
        
        });

		if (name.val().length === 0) {
			nameError.show();
        }

		name.on('keyup', function() {

			if (name.val().length === 0) {

				nameError.show();
			}
			else {

				nameError.hide();
			}

		});

		if (surname.val().length === 0) {
			surnameError.show();
        }

        surname.on('keyup', function() {

			if (surname.val().length === 0) {

				surnameError.show();
			}
			else {

				surnameError.hide();
			}

		});

		if (email.val().length === 0) {
			emailError.show();
        }

		email.on('keyup', function() {

			if (email.val().length === 0) {
				emailError.show();
				emailErrorFormat.hide();
				emailErrorExists.hide();
			} else if (!erEmail.test(email.val())) {
				emailErrorFormat.show();
				emailError.hide(); 
			} else {
				emailError.hide();
				emailErrorFormat.hide();
				$.post("?controller=annuaire&action=check_email", { email: email.val() }, function(email) {
                    if (email.exists) {
                        emailErrorExists.show();
                    } else {
                        emailErrorExists.hide();
                    }
                }, "json");
			}

		});


		if (phone.val().length === 0) {
			phoneError.show();
        }

		phone.on('keyup', function() {

			if (phone.val().length === 0) {
				phoneError.show();
				phoneErrorFormat.hide();
				phoneErrorExists.hide();
			} else if (!erPhone.test(phone.val())) {
				phoneErrorFormat.show();
				phoneError.hide(); 
			} else {
				phoneError.hide();
				phoneErrorFormat.hide();
				$.post("?controller=annuaire&action=check_phone", { phone: phone.val() }, function(phone) {
                    if (phone.exists) {
                        phoneErrorExists.show();
                    } else {
                        phoneErrorExists.hide();
                    }
                }, "json");
			}

		});

		if (selectAnnee[0].selectedIndex === 0) {
            selectErrorAnnee.show();
        }

		selectAnnee.on('click', function() {

			if (selectAnnee[0].selectedIndex === 0) {
				selectErrorAnnee.show();
			}
			else {
				selectErrorAnnee.hide();
			}

		});

		for (let i = 0; i < radioSemestre.length; i++) {

			if (!radioSemestre[i].checked) {
				radioErrorCheckSemestre++;
			}
		}

		if (radioErrorCheckSemestre === radioSemestre.length) {
			radioErrorSemestre.show();
		}


		radioSemestre.on('click', function() {

			radioErrorCheckSemestre = 0;

			for (let i = 0; i < radioSemestre.length; i++) {

				if (!radioSemestre[i].checked) {
					radioErrorCheckSemestre++;
				}
			}

			if (radioErrorCheckSemestre !== radioSemestre.length) {
					radioErrorSemestre.hide();
			}

		});

		if (selectStatut[0].selectedIndex === 0) {
            selectErrorStatut.show();
        }

		selectStatut.on('click', function() {

			if (selectStatut[0].selectedIndex === 0) {
				selectErrorStatut.show();
			}
			else {
				selectErrorStatut.hide();
			}

		});

		if (selectDiscipline[0].selectedIndex === 0) {
            selectErrorDiscipline.show();
        }

		selectDiscipline.on('click', function() {

			if (selectDiscipline[0].selectedIndex === 0) {
				selectErrorDiscipline.show();
			}
			else {
				selectErrorDiscipline.hide();
			}

		});

		if (checkbox.length !== 0) {
            for (let i = 0; i < checkbox.length; i++) {

                if (!checkbox[i].checked) {
                    checkboxErrorCheck++;
                }
            }

            if (checkboxErrorCheck === checkbox.length) {
                checkboxError.show();
            }



            checkbox.on('click', function() {

                let checkboxErrorCheck = 0;

                for (let i = 0; i < checkbox.length; i++) {

                    if (!checkbox[i].checked) {
                        checkboxErrorCheck++;
                    }
                }

                if (checkboxErrorCheck === checkbox.length) {
                    alert('Vous devez cocher au moins une case !');
                    event.preventDefault();
                }
                else {
                    checkboxError.hide();
                }

            });

        }

		for (let i = 0; i < radioDirection.length; i++) {

			if (!radioDirection[i].checked) {
				radioErrorCheckDirection++;
			}
		}

		if (radioErrorCheckDirection === radioDirection.length) {
			radioErrorDirection.show();
		}


		radioDirection.on('click', function() {

			radioErrorCheckDirection = 0;

			for (let i = 0; i < radioDirection.length; i++) {

				if (!radioDirection[i].checked) {
					radioErrorCheckDirection++;
				}
			}

			if (radioErrorCheckDirection !== radioDirection.length) {
				radioErrorDirection.hide();
			}

		});

	});




</script>


<?php require "view_end.php"; ?>
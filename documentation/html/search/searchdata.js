var indexSectionsWithContent =
{
  0: "$0256_abcdefghilmnopqrstuvwxyé–",
  1: "cdemops",
  2: "p",
  3: "cdefgimoprsuv",
  4: "_abcdefghilmnpqrstuvwx",
  5: "$cdefilmsv",
  6: "0256bfnprstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Espaces de nommage",
  3: "Fichiers",
  4: "Fonctions",
  5: "Variables",
  6: "Pages"
};


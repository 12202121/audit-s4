<?php

class Model
{
    /**
     * Attribut contenant l'instance PDO
     */
    private $bd;

    /**
     * Attribut statique qui contiendra l'unique instance de Model
     */
    private static $instance = null;

    /**
     * Constructeur : effectue la connexion à la base de données.
     */
    private function __construct(){
        require "credentials.php";
        $this->bd = new PDO($dsn, $login, $mdp);
        $this->bd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->bd->query("SET nameS 'utf8'");
    }

    /**
     * Méthode permettant de récupérer un modèle car le constructeur est privé (Implémentation du Design Pattern Singleton)
     */
    public static function getModel(){
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Vérifie l'identification de l'utilisateur.
     *
     * Cette fonction vérifie si les identifiants de l'utilisateur (id et mot de passe) sont définis et valides.
     * Si c'est le cas, elle exécute une requête SQL pour récupérer les informations de l'utilisateur.
     *
     * @return array|false Retourne un tableau associatif contenant les données de l'utilisateur (id, mot de passe, fonction)
     * ou false si les identifiants sont invalides ou absents.
     */
    public function identification_Check() {
        // Vérifie si l'identifiant et le mot de passe sont définis et valides
        if (isset($_POST["id"]) && preg_match("/^[0-9]+$/", $_POST["id"]) && isset($_POST["mdp"]) && $_POST["mdp"] != "") {
            // Prépare la requête SQL
            $requete = $this->bd->prepare("SELECT id_personne AS id, motDePasse AS mdp, grade(:id) AS fonction FROM personne WHERE id_personne = :id");
            $requete->bindValue(":id", $_POST["id"]);
            
            // Exécute la requête
            $requete->execute();
            
            // Récupère le résultat
            $res = $requete->fetch(PDO::FETCH_ASSOC);
            
            // Retourne les données de l'utilisateur
            return $res;
        }
        
        // Retourne false si les identifiants sont invalides ou absents
        return false;
    }


    /**
     * Ajoute un nouvel utilisateur à la base de données.
     *
     * Cette fonction insère les informations de l'utilisateur dans la table `personne`. Si l'utilisateur est un enseignant, 
     * elle insère également les informations spécifiques dans les tables `enseignant`, `assigner`, et éventuellement `equipedirection`.
     * Si l'utilisateur est un secrétaire, elle insère les informations dans la table `secretaire`.
     *
     * @param array $infos Un tableau associatif contenant les informations de l'utilisateur :
     * - 'id' : Identifiant de l'utilisateur.
     * - 'nom' : Nom de l'utilisateur.
     * - 'prenom' : Prénom de l'utilisateur.
     * - 'email' : Email de l'utilisateur.
     * - 'phone' : Numéro de téléphone de l'utilisateur.
     * - 'mdp' : Mot de passe de l'utilisateur.
     * - 'poste' : Poste de l'utilisateur ('enseignant' ou autre).
     * - 'discipline' : Discipline de l'utilisateur (si enseignant).
     * - 'statut' : Statut de l'utilisateur (si enseignant).
     * - 'annee' : Année académique.
     * - 'departements' : Départements associés (si enseignant).
     * - 'semestre' : Semestre d'affectation (si enseignant).
     * - 'direction' : Booléen indiquant si l'utilisateur est dans l'équipe de direction (si enseignant).
     */
    public function ajouterUtilisateur($infos) {
        // Insère les informations de base dans la table `personne`
        $requete = $this->bd->prepare('INSERT INTO personne (id_personne, nom, prenom, email, phone, motDePasse) VALUES (:id, :nom, :prenom, :email, :phone, :mdp)');
        $requete->bindValue(":id", $infos["id"]);
        $requete->bindValue(":nom", $infos["nom"]);
        $requete->bindValue(":prenom", $infos["prenom"]);
        $requete->bindValue(":email", $infos["email"]);
        $requete->bindValue(":phone", $infos["phone"]);
        $requete->bindValue(":mdp", $infos["mdp"]);
        $requete->execute();

        if ($infos["poste"] == "enseignant") {
            // Insère les informations spécifiques à un enseignant dans la table `enseignant`
            $requete = $this->bd->prepare("INSERT INTO enseignant (id_personne, idDiscipline, id_categorie, AA) VALUES (:id, (SELECT idDiscipline FROM discipline WHERE libelledisc = :disc), (SELECT id_categorie FROM categorie WHERE siglecat = :statut), :annee)");
            $requete->bindValue(":id", $infos["id"]);
            $requete->bindValue(":disc", $infos["discipline"]);
            $requete->bindValue(":statut", $infos["statut"]);
            $requete->bindValue(":annee", $infos["annee"]);
            $requete->execute();

            // Insère les départements associés à l'enseignant dans la table `assigner`
            if (isset($infos["departements"])) {
                foreach ($infos["departements"] as $v) {
                    $requete = $this->bd->prepare("INSERT INTO assigner (id_personne, idDepartement, AA, S) VALUES (:id, (SELECT idDepartement FROM departement WHERE libelledept = :libDept), :annee, :semestre)");
                    $requete->bindValue(":id", $infos["id"]);
                    $requete->bindValue(":libDept", $v);
                    $requete->bindValue(":annee", $infos["annee"]);
                    $requete->bindValue(":semestre", $infos["semestre"]);
                    $requete->execute();
                }
            }

            // Insère l'utilisateur dans l'équipe de direction si applicable
            if ($infos["direction"] == "true") {
                $requete = $this->bd->prepare("INSERT INTO equipedirection (id_personne) VALUES (:id)");
                $requete->bindValue(":id", $infos["id"]);
                $requete->execute();
            }
        } else {
            // Insère les informations spécifiques à un secrétaire dans la table `secretaire`
            $requete = $this->bd->prepare("INSERT INTO secretaire (id_personne) VALUES (:id)");
            $requete->bindValue(":id", $infos["id"]);
            $requete->execute();
        }
    }


    /**
     * Vérifie si un identifiant existe dans la base de données.
     *
     * Cette fonction vérifie si l'identifiant donné existe dans la table `personne`.
     *
     * @param int $id L'identifiant à vérifier.
     * @return bool Retourne true si l'identifiant existe dans la base de données, sinon false.
     */
    public function id_in_db($id) {
        // Prépare la requête SQL pour vérifier l'existence de l'identifiant
        $requete = $this->bd->prepare('SELECT * FROM personne WHERE :id = id_personne');
        $requete->bindValue(":id", $id);
        
        // Exécute la requête
        $requete->execute();
        
        // Récupère le résultat
        $res = $requete->fetch(PDO::FETCH_NUM);
        
        // Retourne true si l'identifiant existe, sinon false
        if ($res != false) {
            return true;
        }
        return false;
    }

    /**
     * Vérifie si un email existe dans la base de données.
     *
     * Cette fonction vérifie si l'email donné existe dans la table `personne`.
     *
     * @param int $email L'email à vérifier.
     * @return bool Retourne true si l'email existe dans la base de données, sinon false.
     */

    public function email_in_db($email) {
        // Prépare la requête SQL pour vérifier l'existence de l'email
        $requete = $this->bd->prepare('SELECT * FROM personne WHERE :email = email');
        $requete->bindValue(":email", $email);
        
        // Exécute la requête
        $requete->execute();
        
        // Récupère le résultat
        $res = $requete->fetch(PDO::FETCH_NUM);
        
        // Retourne true si l'email existe, sinon false
        if ($res != false) {
            return true;
        }
        return false;
    }

    /**
     * Vérifie si un numéro de téléphone existe dans la base de données.
     *
     * Cette fonction vérifie si le numéro de téléphone donné existe dans la table `personne`.
     *
     * @param int $phone Le numéro de téléphone à vérifier.
     * @return bool Retourne true si l'email existe dans la base de données, sinon false.
     */

     public function phone_in_db($phone) {
        // Prépare la requête SQL pour vérifier l'existence du numéro de téléphone
        $requete = $this->bd->prepare('SELECT * FROM personne WHERE :phone = phone');
        $requete->bindValue(":phone", $phone);
        
        // Exécute la requête
        $requete->execute();
        
        // Récupère le résultat
        $res = $requete->fetch(PDO::FETCH_NUM);
        
        // Retourne true si le numéro de téléphone existe, sinon false
        if ($res != false) {
            return true;
        }
        return false;
    }




    /**
     * Récupère les catégories, disciplines et départements depuis la base de données.
     *
     * Cette fonction exécute trois requêtes pour récupérer les données des tables `categorie`, `discipline` et `departement`.
     * Les résultats sont stockés dans un tableau associatif avec les clés 'statut', 'discipline', et 'departements'.
     *
     * @return array Un tableau associatif contenant les listes de catégories, disciplines et départements.
     * - 'statut' : Liste des catégories (sigleCat).
     * - 'discipline' : Liste des disciplines (libelleDisc).
     * - 'departements' : Liste des départements (libelleDept).
     */
    public function getCatDiscDpt() {
        // Récupère les catégories
        $requete = $this->bd->prepare('SELECT sigleCat FROM categorie');
        $requete->execute();
        $data["statut"] = $requete->fetchAll(PDO::FETCH_ASSOC);

        // Récupère les disciplines
        $requete = $this->bd->prepare('SELECT libelleDisc FROM discipline');
        $requete->execute();
        $data["discipline"] = $requete->fetchAll(PDO::FETCH_ASSOC);

        // Récupère les départements
        $requete = $this->bd->prepare('SELECT libelleDept FROM departement');
        $requete->execute();
        $data["departements"] = $requete->fetchAll(PDO::FETCH_ASSOC);

        // Retourne les données
        return $data;
    }

    /**
     * Récupère une liste de personnes depuis la base de données avec pagination et option de recherche.
     *
     * Cette fonction récupère une liste de personnes en fonction des paramètres de pagination et de recherche.
     * Si un terme de recherche est fourni, elle filtre les résultats par nom ou prénom. Les résultats sont
     * triés par nom.
     *
     * @param int $offset Le décalage pour la pagination. Défaut à 0.
     * @param int $limit Le nombre maximum de résultats à retourner. Défaut à 50.
     * @param string $order Le champ par lequel trier les résultats. Défaut à "nom".
     * @return array Un tableau associatif contenant les résultats de la requête, avec les clés :
     * - 'id_personne' : Identifiant de la personne.
     * - 'nom' : Nom de la personne.
     * - 'prenom' : Prénom de la personne.
     * - 'fonction' : Fonction de la personne (résultat de la fonction `grade`).
     */
    public function getList($offset = 0, $limit = 50, $order = "nom") {
        // Vérifie si un terme de recherche est défini et non vide
        if (isset($_GET["recherche"]) && !preg_match("/^ *$/", $_GET["recherche"])) {
            // Prépare la requête SQL avec filtre de recherche
            $requete = $this->bd->prepare("SELECT id_personne, nom, prenom, grade(id_personne) AS fonction FROM personne WHERE nom ~* :rech OR prenom ~* :rech ORDER BY $order LIMIT :limit OFFSET :offset");
            $requete->bindValue(":rech", $_GET["recherche"], PDO::PARAM_STR);
        } else {
            // Prépare la requête SQL sans filtre de recherche
            $requete = $this->bd->prepare("SELECT id_personne, nom, prenom, grade(id_personne) AS fonction FROM personne ORDER BY $order LIMIT :limit OFFSET :offset");
        }

        // Lie les paramètres de pagination
        $requete->bindValue(':limit', $limit, PDO::PARAM_INT);
        $requete->bindValue(':offset', $offset, PDO::PARAM_INT);

        // Exécute la requête
        $requete->execute();

        // Retourne les résultats de la requête
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
    * Récupère les informations de profil d'un utilisateur.
    *
    * Cette fonction récupère les informations de base d'un utilisateur depuis la table `personne`.
    * Si l'utilisateur n'est pas un secrétaire ou une personne sans fonction, elle récupère également
    * les informations spécifiques de l'enseignant, y compris sa discipline, son statut et ses départements.
    *
    * @param int $id L'identifiant de l'utilisateur.
    * @return array Un tableau associatif contenant les informations du profil de l'utilisateur :
    * - 'id' : Identifiant de l'utilisateur.
    * - 'nom' : Nom de l'utilisateur.
    * - 'prenom' : Prénom de l'utilisateur.
    * - 'email' : Email de l'utilisateur.
    * - 'phone' : Numéro de téléphone de l'utilisateur.
    * - 'fonction' : Fonction de l'utilisateur.
    * - 'discipline' : Discipline de l'utilisateur (si enseignant).
    * - 'statut' : Statut de l'utilisateur (si enseignant).
    * - 'depts' : Liste des départements de l'utilisateur (si enseignant).
    */
    public function getInfoProfil($id) {
    // Prépare la requête pour récupérer les informations de base de l'utilisateur
    $requete = $this->bd->prepare("SELECT id_personne as id, nom, prenom, email, phone, grade(:id) AS fonction FROM personne WHERE id_personne = :id");
    $requete->bindValue(':id', $id);
    $requete->execute();
    $infos = $requete->fetch(PDO::FETCH_ASSOC);

    // Si l'utilisateur n'est pas un secrétaire ou une personne sans fonction, récupère les informations spécifiques de l'enseignant
    if ($infos["fonction"] != "secretaire" && $infos["fonction"] != "personne") {
        // Récupère la discipline et le statut de l'enseignant
        $requete = $this->bd->prepare("SELECT libelledisc AS discipline, siglecat AS statut FROM enseignant 
            JOIN categorie USING (id_categorie) JOIN discipline USING (idDiscipline) WHERE id_personne = :id");
        $requete->bindValue(':id', $id);
        $requete->execute();
        $infos = array_merge($infos, $requete->fetch(PDO::FETCH_ASSOC));

        // Récupère les départements de l'enseignant
        $requete = $this->bd->prepare("SELECT libelleDept AS depts FROM assigner 
            JOIN departement USING (idDepartement) WHERE assigner.id_personne = :id");
        $requete->bindValue(":id", $id);
        $requete->execute();
        $infos["depts"] = $requete->fetchAll(PDO::FETCH_ASSOC);
    }

    // Retourne les informations du profil de l'utilisateur
    return $infos;
    }


    /**
     * Supprime un utilisateur de la base de données.
     *
     * Cette fonction supprime un utilisateur de la table `personne` en fonction de son identifiant.
     *
     * @param int $id L'identifiant de l'utilisateur à supprimer.
     * @return void
     */
    public function supprimerUtilisateur($id) {
        // Prépare la requête SQL pour supprimer l'utilisateur
        $requete = $this->bd->prepare("DELETE FROM personne WHERE id_personne = :id");
        $requete->bindValue(':id', $id, PDO::PARAM_INT);

        // Exécute la requête
        $requete->execute();
    }

    /**
     * Récupère les informations sur le département associé à un utilisateur.
     *
     * Cette fonction récupère les informations sur le département associé à un utilisateur depuis la table `departement`
     * en fonction de l'identifiant de l'utilisateur.
     *
     * @param int $id L'identifiant de l'utilisateur.
     * @return array|null Un tableau associatif contenant les informations sur le département associé à l'utilisateur,
     * ou null si aucune information n'est trouvée.
     */
    public function getInfoDepartement($id) {
        // Prépare la requête SQL pour récupérer les informations sur le département
        $requete = $this->bd->prepare("SELECT * FROM departement WHERE id_personne = :id");
        $requete->bindValue(':id', $id, PDO::PARAM_INT);
        
        // Exécute la requête
        $requete->execute();
        
        // Récupère les informations sur le département
        $infos = $requete->fetch(PDO::FETCH_ASSOC);
        
        // Retourne les informations sur le département ou null si aucune information n'est trouvée
        return $infos;
    }

    /**
     * Met à jour le profil d'un utilisateur dans la base de données.
     *
     * Cette fonction met à jour les informations du profil d'un utilisateur dans la table `personne`.
     * Elle permet de mettre à jour le nom, le prénom, l'email, le téléphone et éventuellement le mot de passe
     * de l'utilisateur. Si l'utilisateur est un enseignant, elle met également à jour sa discipline, son statut
     * et ses départements associés.
     *
     * @param array $infos Un tableau associatif contenant les informations à mettre à jour pour l'utilisateur :
     * - 'id' : Identifiant de l'utilisateur.
     * - 'nom' : Nouveau nom de l'utilisateur.
     * - 'prenom' : Nouveau prénom de l'utilisateur.
     * - 'email' : Nouvel email de l'utilisateur.
     * - 'phone' : Nouveau numéro de téléphone de l'utilisateur.
     * - 'mdp' : Nouveau mot de passe de l'utilisateur (peut être null pour ne pas le mettre à jour).
     * - 'fonction' : Fonction de l'utilisateur.
     * - 'discipline' : Nouvelle discipline de l'utilisateur (si enseignant).
     * - 'statut' : Nouveau statut de l'utilisateur (si enseignant).
     * - 'departements' : Nouveaux départements associés à l'utilisateur (si enseignant).
     * - 'annee' : Année académique associée (si enseignant).
     * - 'semestre' : Nouveau semestre associé (si enseignant).
     * @return void
     */
    public function updateProfil($infos) {
        if ($infos['mdp'] == null) {
            // Met à jour les informations de base de l'utilisateur sans modifier le mot de passe
            $requete = $this->bd->prepare("UPDATE personne SET nom = :nom, prenom = :prenom, email = :email, phone = :phone WHERE id_personne = :id");
            $requete->bindValue(":id", $infos["id"]);
            $requete->bindValue(":nom", $infos["nom"]);
            $requete->bindValue(":prenom", $infos["prenom"]);
            $requete->bindValue(":email", $infos["email"]);
            $requete->bindValue(":phone", $infos["phone"]);
            $requete->execute();
        } else {
            // Met à jour les informations de base de l'utilisateur en incluant le mot de passe
            $requete = $this->bd->prepare("UPDATE personne SET nom = :nom, prenom = :prenom, email = :email, phone = :phone, motdepasse = :mdp WHERE id_personne = :id");
            $requete->bindValue(":id", $infos["id"]);
            $requete->bindValue(":nom", $infos["nom"]);
            $requete->bindValue(":prenom", $infos["prenom"]);
            $requete->bindValue(":email", $infos["email"]);
            $requete->bindValue(":phone", $infos["phone"]);
            $requete->bindValue(":mdp", $infos["mdp"]);
            $requete->execute();
        }
        
        // Si l'utilisateur est un enseignant, met à jour les informations spécifiques à l'enseignant
        if ($infos["fonction"] != "secretaire" && $infos["fonction"] != "personne") {
            // Met à jour la discipline et le statut de l'enseignant
            $requete = $this->bd->prepare("UPDATE enseignant SET idDiscipline = (SELECT idDiscipline FROM discipline WHERE libelledisc = :disc), id_categorie = (SELECT id_categorie FROM categorie WHERE siglecat = :statut) WHERE id_personne = :id");
            $requete->bindValue(":id", $infos["id"]);
            $requete->bindValue(":disc", $infos["discipline"]);
            $requete->bindValue(":statut", $infos["statut"]);
            $requete->execute();
            
            // Supprime les affectations précédentes de département pour cet utilisateur
            $requete = $this->bd->prepare("DELETE FROM assigner WHERE id_personne = :id");
            $requete->bindValue(":id", $infos["id"]);
            $requete->execute();
            
            // Ajoute les nouvelles affectations de département pour cet utilisateur
            if (isset($infos["departements"])) {
                foreach ($infos["departements"] as $v) {
                    $requete = $this->bd->prepare("INSERT INTO assigner (id_personne, idDepartement, AA, S) VALUES (:id, (SELECT idDepartement FROM departement WHERE libelledept = :libDept), :annee, :semestre)");
                    $requete->bindValue(":id", $infos["id"]);
                    $requete->bindValue(":libDept", $v);
                    $requete->bindValue(":annee", $infos["annee"]);
                    $requete->bindValue(":semestre", $infos["semestre"]);
                    $requete->execute();
                }
            }
        }
    }

    /**
     * Récupère les semestres disponibles depuis la base de données.
     *
     * Cette fonction récupère les semestres disponibles dans la table `semestre`.
     *
     * @return array Un tableau associatif contenant les semestres disponibles.
     */
    public function getSemestre() {
        // Prépare la requête SQL pour récupérer les semestres distincts
        $requete = $this->bd->prepare("SELECT DISTINCT S FROM semestre");
        
        // Exécute la requête
        $requete->execute();
        
        // Retourne les semestres disponibles
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Récupère les noms des départements depuis la base de données.
     *
     * Cette fonction récupère les noms des départements et leurs identifiants depuis la table `departement`.
     *
     * @return array Un tableau associatif contenant les noms des départements et leurs identifiants.
     */
    public function getNomDepartement() {
        // Prépare la requête SQL pour récupérer les noms des départements et leurs identifiants
        $requete = $this->bd->prepare("SELECT libelleDept, idDepartement AS id FROM departement");
        
        // Exécute la requête
        $requete->execute();
        
        // Retourne les noms des départements et leurs identifiants
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Récupère les informations sur un département depuis la base de données.
     *
     * Cette fonction récupère les informations sur un département spécifique en fonction de son identifiant
     * depuis la table `departement`.
     *
     * @param int $id L'identifiant du département à récupérer.
     * @return array|null Un tableau associatif contenant les informations sur le département spécifié,
     * ou null si aucune information n'est trouvée.
     */
    public function getInfoDepartement2($id) {
        // Prépare la requête SQL pour récupérer les informations sur le département
        $requete = $this->bd->prepare("SELECT * FROM departement WHERE idDepartement = :id");
        $requete->bindValue(':id', $id, PDO::PARAM_INT);
        
        // Exécute la requête
        $requete->execute();
        
        // Récupère les informations sur le département
        $infos = $requete->fetch(PDO::FETCH_ASSOC);
        
        // Retourne les informations sur le département ou null si aucune information n'est trouvée
        return $infos;
    }

    /**
     * Récupère les formations disponibles depuis la base de données.
     *
     * Cette fonction récupère les identifiants et noms des formations depuis la table `formation`.
     *
     * @return array Un tableau associatif contenant les identifiants et noms des formations disponibles.
     */
    public function getFormation() {
        // Prépare la requête SQL pour récupérer les formations
        $requete = $this->bd->prepare('SELECT idFormation AS id, nom FROM formation');
        
        // Exécute la requête
        $requete->execute();
        
        // Retourne les formations disponibles
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Récupère les disciplines disponibles depuis la base de données.
     *
     * Cette fonction récupère les identifiants et noms des disciplines depuis la table `discipline`.
     *
     * @return array Un tableau associatif contenant les identifiants et noms des disciplines disponibles.
     */
    public function getDiscipline() {
        // Prépare la requête SQL pour récupérer les disciplines
        $requete = $this->bd->prepare('SELECT idDiscipline AS id, libelleDisc AS nom FROM discipline');
        
        // Exécute la requête
        $requete->execute();
        
        // Retourne les disciplines disponibles
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }
        
    /**
     * Ajoute un besoin dans la base de données.
     *
     * Cette fonction ajoute un besoin dans la table `besoin` en fonction des données fournies via POST.
     * Elle supprime d'abord les besoins existants correspondant aux critères spécifiés, puis ajoute le nouveau besoin.
     *
     * @return void
     */
    public function ajouterBesoin() {
        // Supprime les besoins existants correspondant aux critères spécifiés
        $requete = $this->bd->prepare('DELETE FROM besoin WHERE aa = :annee AND s = :semestre AND iddepartement = (SELECT iddepartement FROM departement WHERE id_personne = :id) AND iddiscipline = :discipline AND idformation = :form');
        $requete->bindValue(":annee", $_POST["annee"]);
        $requete->bindValue(":semestre", $_POST["semestre"]);
        $requete->bindValue(":id", $_SESSION["id"]);
        $requete->bindValue(":discipline", $_POST["discipline"]);
        $requete->bindValue(":form", $_POST["formation"]);
        $requete->execute();

        // Ajoute le nouveau besoin
        $req = $this->bd->prepare('INSERT INTO besoin VALUES (:aa, :s, :idformation, :iddiscipline, (SELECT iddepartement FROM departement WHERE id_personne = :id), :besoin)');
        $req->bindValue(":aa", $_POST["annee"]);
        $req->bindValue(":s", $_POST["semestre"]);
        $req->bindValue(":idformation", $_POST["formation"]);
        $req->bindValue(":id", $_SESSION["id"]);
        $req->bindValue(":iddiscipline", $_POST["discipline"]);
        $req->bindValue(":besoin", $_POST["besoin"]);
        $req->execute(); 
    }

    /**
     * Ajoute des heures d'enseignement dans la base de données.
     *
     * Cette fonction ajoute des heures d'enseignement pour un enseignant donné dans la table `enseigne`.
     * Les heures sont associées à un département, une discipline, une année académique et un semestre.
     *
     * @return void
     */
    public function ajouterHeure() {
        // Prépare la requête SQL pour ajouter les heures d'enseignement
        $requete = $this->bd->prepare('INSERT INTO enseigne (id_personne, idDiscipline, idDepartement, AA, S, nbHeureEns, typeH) VALUES (:id, (SELECT idDiscipline FROM enseignant WHERE id_personne = :id), :idDpt, :aa, :s, :nbHE, :typeH)');
        
        // Lie les valeurs aux paramètres de la requête
        $requete->bindValue(":id", $_SESSION["id"]);
        $requete->bindValue(":aa", $_POST["annee"]);
        $requete->bindValue(":s", $_POST["semestre"]);
        $requete->bindValue(":nbHE", $_POST["heure"]);
        $requete->bindValue(":typeH", $_POST["type_h"]);
        $requete->bindValue(":idDpt", $_POST["dept"]);
        
        // Exécute la requête
        $requete->execute();
    }

    /**
     * Récupère les années académiques disponibles depuis la base de données.
     *
     * Cette fonction récupère les années académiques disponibles dans la table `annee`.
     *
     * @return array Un tableau associatif contenant les années académiques disponibles.
     */
    public function getAnnee() {
        // Prépare la requête SQL pour récupérer les années académiques
        $requete = $this->bd->prepare("SELECT AA FROM annee");
        
        // Exécute la requête
        $requete->execute();
        
        // Retourne les années académiques disponibles
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Récupère les départements disponibles depuis la base de données.
     *
     * Cette fonction récupère les identifiants et noms des départements depuis la table `departement`.
     *
     * @return array Un tableau associatif contenant les identifiants et noms des départements disponibles.
     */
    public function getDpt() {
        // Prépare la requête SQL pour récupérer les départements
        $requete = $this->bd->prepare('SELECT idDepartement AS id, libelleDept AS nom FROM departement');
        
        // Exécute la requête
        $requete->execute();
        
        // Retourne les départements disponibles
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Récupère les noms des formations proposées par un département spécifique depuis la base de données.
     *
     * Cette fonction récupère les noms des formations proposées par un département donné depuis la table `propose` et `formation`.
     *
     * @param int $id L'identifiant du département.
     * @return array Un tableau associatif contenant les noms des formations proposées par le département spécifié.
     */
    public function getNomFormationPropose($id) {
        // Prépare la requête SQL pour récupérer les noms des formations proposées par le département
        $requete = $this->bd->prepare('SELECT nom from propose join formation using (idformation) where iddepartement = :id');
        $requete->bindValue(':id', $id);
        
        // Exécute la requête
        $requete->execute();
        
        // Retourne les noms des formations proposées par le département spécifié
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Récupère l'effectif d'un département donné depuis la base de données.
     *
     * Cette fonction récupère le nombre d'assignations dans un département spécifique depuis la table `assigner`.
     *
     * @param int $id L'identifiant du département.
     * @return int Le nombre d'assignations dans le département spécifié.
     */
    public function getEffectifDpt($id) {
        // Prépare la requête SQL pour récupérer l'effectif du département
        $requete = $this->bd->prepare('SELECT count(*) as nb from assigner where iddepartement = :id');
        $requete->bindValue(':id', $id);
        
        // Exécute la requête
        $requete->execute();
        
        // Récupère le résultat de la requête et retourne le nombre d'assignations
        $res = $requete->fetch(PDO::FETCH_ASSOC);
        return $res['nb'];
    }

    /**
     * Récupère le besoin en heures d'un département donné depuis la base de données.
     *
     * Cette fonction récupère la somme des besoins en heures dans un département spécifique depuis la table `besoin`.
     *
     * @param int $id L'identifiant du département.
     * @return array Le besoin en heures dans le département spécifié.
     */
    public function getBesoinHeureDpt($id) {
        // Prépare la requête SQL pour récupérer le besoin en heures du département
        $requete = $this->bd->prepare('SELECT SUM(besoin_heure) from besoin where iddepartement = :id');
        $requete->bindValue(':id', $id);
        
        // Exécute la requête
        $requete->execute();
        
        // Récupère le résultat de la requête et retourne le besoin en heures
        $res = $requete->fetch(PDO::FETCH_ASSOC);
        return $res;
    }

    /**
     * Récupère les demandes de besoin depuis la base de données.
     *
     * Cette fonction récupère les demandes de besoin en heures depuis la table `besoin`, `demandes`, `personne`, `departement` et `discipline`.
     * Si l'utilisateur est un chef de département, seules les demandes de son département sont récupérées.
     *
     * @return array Un tableau associatif contenant les demandes de besoin en heures.
     */
    public function getDemande() {
        // Prépare la requête SQL pour récupérer les demandes de besoin
        $requete = $this->bd->prepare('SELECT * from besoin JOIN demandes USING (idDepartement) JOIN personne USING (id_personne) JOIN departement using (iddepartement) JOIN discipline using (iddiscipline)');
        
        // Si l'utilisateur est un chef de département, restreint les demandes à son département
        if ($_SESSION['permission'] == 'chefdedpt') {
            $requete = $this->bd->prepare('SELECT * from besoin JOIN demandes USING (idDepartement) JOIN personne USING (id_personne) JOIN departement using (iddepartement) JOIN discipline using (iddiscipline) where demandes.id_personne = :id');
            $requete->bindValue(':id', $_SESSION['id']);
        }
        
        // Exécute la requête
        $requete->execute();
        
        // Retourne les demandes de besoin en heures
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Récupère les logs depuis la base de données.
     *
     * Cette fonction récupère tous les logs depuis la table `log`, ordonnés par date de modification décroissante.
     *
     * @return array Un tableau associatif contenant les logs.
     */
    public function getLog() {
        // Prépare la requête SQL pour récupérer les logs
        $requete = $this->bd->prepare('SELECT * from log ORDER BY date_modif DESC');
        
        // Exécute la requête
        $requete->execute();
        
        // Retourne les logs
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Récupère les heures d'enseignement d'un utilisateur selon le filtre spécifié.
     *
     * Cette fonction récupère les heures d'enseignement d'un utilisateur selon le filtre spécifié, tel que par année académique, semestre, département ou type d'heure.
     *
     * @param int $id L'identifiant de l'utilisateur.
     * @return array Un tableau associatif contenant les heures d'enseignement selon le filtre spécifié.
     */
    public function getHeure($id) {
        // Vérifie s'il y a un filtre spécifié
        if (isset($_POST["filter"])) {
            // Détermine la requête SQL en fonction du filtre spécifié
            if ($_POST["filter"] == "annee") {
                $requete = $this->bd->prepare('SELECT AA AS label, SUM(nbHeureEns) AS heures from enseigne JOIN departement USING (idDepartement) where enseigne.id_personne = :id GROUP BY AA');
            } elseif ($_POST["filter"] == "semestre") {
                $requete = $this->bd->prepare('SELECT S AS label, SUM(nbHeureEns) AS heures from enseigne JOIN departement USING (idDepartement) where enseigne.id_personne = :id GROUP BY S');
            } elseif ($_POST["filter"] == "departement") {
                $requete = $this->bd->prepare('SELECT libelleDept AS label, SUM(nbHeureEns) AS heures from enseigne JOIN departement USING (idDepartement) where enseigne.id_personne = :id GROUP BY libelleDept');
            } else {
                $requete = $this->bd->prepare('SELECT typeH AS label, SUM(nbHeureEns) AS heures from enseigne JOIN departement USING (idDepartement) where enseigne.id_personne = :id GROUP BY typeH');
            }
        } else {
            // Si aucun filtre spécifié, utilise le filtre par type d'heure par défaut
            $requete = $this->bd->prepare('SELECT typeH AS label, SUM(nbHeureEns) AS heures from enseigne JOIN departement USING (idDepartement) where enseigne.id_personne = :id GROUP BY typeH');
        }
        
        // Lie la valeur de l'identifiant à la requête
        $requete->bindValue(':id', $id);
        
        // Exécute la requête
        $requete->execute();
        
        // Retourne les heures d'enseignement selon le filtre spécifié
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }

    
    /**
     * Récupère les données concernant l'IUT selon les filtres spécifiés.
     *
     * Cette fonction récupère les données de l'IUT selon les filtres spécifiés, tels que par année académique, semestre, département, discipline ou formation.
     * Les données peuvent être filtrées par le nombre d'heures ou par le statut.
     *
     * @return array Un tableau associatif contenant les données de l'IUT en fonction des filtres.
     */
   public function getIUT(){
        if(isset($_POST["filter"]) and isset($_POST["choix"])){
            if($_POST["choix"]=="annee"){
                if($_POST["filter"]=="heures"){
                    $requete = $this->bd->prepare('SELECT AA AS label, SUM(nbHeureEns) AS nb from enseigne JOIN departement USING (idDepartement) GROUP BY AA ORDER BY AA');
                }
                if($_POST["filter"]=="statut"){
                    $requete = $this->bd->prepare('SELECT AA AS label, sigleCat AS sigle, count(sigleCat) AS nb from enseignant JOIN categorie USING (id_categorie) GROUP BY AA,sigleCat ORDER BY AA');
                }
            }
            elseif($_POST["choix"]=="semestre"){
                if($_POST["filter"]=="heures"){
                    $requete = $this->bd->prepare('SELECT AA AS label, S AS sigle, SUM(nbHeureEns) AS nb from enseigne JOIN departement USING (idDepartement) GROUP BY S,AA ORDER BY AA');
                }   
                if($_POST["filter"]=="statut"){
                    $requete = $this->bd->prepare('SELECT sigleCat AS label, S AS sigle, count(sigleCat) AS nb from enseignant JOIN categorie USING (id_categorie) JOIN semestre USING (AA) WHERE AA = 2024 GROUP BY S,sigleCat');
                }
            }
            elseif($_POST["choix"]=="departement"){
                if($_POST["filter"]=="heures"){
                    $requete = $this->bd->prepare('SELECT AA AS sigle, libelleDept AS label, SUM(nbHeureEns) AS nb from enseigne JOIN departement USING (idDepartement) GROUP BY libelleDept,AA ORDER BY AA');
                }
                if($_POST["filter"]=="statut"){
                    $requete = $this->bd->prepare('SELECT libelleDept AS label, sigleCat AS sigle ,count(sigleCat) AS nb from enseignant JOIN categorie USING (id_categorie) JOIN enseigne USING (id_personne) JOIN departement USING (idDepartement) WHERE enseigne.AA = 2024 GROUP BY sigleCat,libelleDept');
                }
            }
            elseif($_POST["choix"]=="discipline"){
                if($_POST["filter"]=="heures"){
                    $requete = $this->bd->prepare('SELECT libelleDisc AS label, AA AS sigle , SUM(nbHeureEns) AS nb from enseigne JOIN discipline USING (idDiscipline) GROUP BY AA,libelleDisc ORDER BY AA');
                }
                if($_POST["filter"]=="statut"){
                    $requete = $this->bd->prepare('SELECT libelleDisc AS label, sigleCat AS sigle, count(sigleCat) AS nb from enseignant JOIN categorie USING (id_categorie) JOIN  discipline USING (idDiscipline) GROUP BY sigleCat,libelleDisc');
                }
            }
            elseif($_POST["choix"]=="formation"){
                if($_POST["filter"]=="heures"){
                    $requete = $this->bd->prepare('SELECT AA as sigle, nom AS label, SUM(besoin_heure) AS nb FROM besoin JOIN formation USING (idFormation) GROUP BY AA,nom ORDER BY AA');
                }
                if($_POST["filter"]=="statut"){
                $requete = $this->bd->prepare('SELECT nom AS label, sigleCat AS sigle, count(sigleCat) AS nb from enseignant JOIN categorie USING (id_categorie) JOIN enseigne USING (id_personne) JOIN departement USING (idDepartement) JOIN propose USING (idDepartement) JOIN formation USING (idFormation) WHERE enseigne.AA = 2024 GROUP BY nom,sigleCat,libelleDept');
                }
            }
        }
        else{
            $requete = $this->bd->prepare('SELECT AA AS label, SUM(nbHeureEns) AS nb from enseigne JOIN departement USING (idDepartement) GROUP BY AA ORDER BY AA');
        }
        $requete->execute();
        return $requete->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Vérifie si le mail de l'utilisateur existe
     *
     * Cette fonction vérifie si les identifiants de l'utilisateur (email) sont définis et valides.
     * Si c'est le cas, elle exécute une requête SQL pour récupérer les informations de l'utilisateur.
     *
     * @return string|false Retourne une chaîne contenant le mot de passe de l'utilisateur
     * ou false si les identifiants sont invalides ou absents.
     */
    public function email_Check($email) {
        
        // Prépare la requête SQL
        $requete = $this->bd->prepare("SELECT motdepasse FROM personne WHERE email = :email");
        $requete->bindValue(":email", $email);
        
        // Exécute la requête
        $requete->execute();
        
        // Récupère le résultat
        $res = $requete->fetch(PDO::FETCH_ASSOC);
        
        // Vérifie si le résultat existe et retourne le mot de passe ou false si le résultat est vide
        if ($res) {
            return $res['motdepasse'];
        } else {
            return false;
        }
    }
    public function change_mdp($infos){
        // Met à jour les informations de base de l'utilisateur en incluant le mot de passe
        $requete = $this->bd->prepare("UPDATE personne SET motdepasse = :mdp WHERE email = :email");
        $requete->bindValue(":email", $infos["email"]);
        $requete->bindValue(":mdp", $infos["mdp"]);
        $requete->execute();
    }
}

## Projet BASMOS 

C'est un site intranet dynamique qui sert comme outil de pilotage des heures des ensenseignants de l'IUT de Villetnaeuse.

Il permet de faciliter la planification et la gestion des heures d’enseignement en offrant des fonctionnalités adaptées aux besoins de chaque département et aux divers niveaux de responsabilités au sein de l'IUT.

Ce projet a été initialement développé par le groupe De Evolution. Notre objectif est de l'améliorer en ajoutant de nouvelles fonctionnalités, en optimisant les performances et en améliorant l'interface utilisateur.

https://jupyter.univ-paris13.fr/~12206358/di-evoluzion-audit/

[Les membres de l'équipe](https://gitlab.sorbonne-paris-nord.fr/basmos/di-evoluzion-audit/-/blob/12213367-main-patch-50912/README.md#l%C3%A9quipe-basmos)

## Les fonctionnalités de notre site 

### Gestion des utilisateurs  

**L'accés** à la plateforme est fait uniquement après **authentification**. 

**Exemples :**

Pour se connecter en tant que :
* Admin qui a tous les droits =>  Identifiant : `123` , PASSWORD : `Topaze` 

* Enseignant qui peut que consulter le site => Identifiant : `14` , PASSWORD : `Topaze`   

**Rôle et permission :** 

* `Enseignant` : Consultation uniquement des données saisies. 

* `Chefs de département`: Consultation et modification des besoins en heures de leur département.

* `Directeur` et `Administarteur` : Consultation et modification des besoins des départements, gestion des données des enseignants.

**visualisation des données :** 
* Données globales de l'IUT.
* Données par département.
* Données par année.
* Données par formation.
* Données par discipline.
* Données par catégorie d’agent.

**Calcul du taux d'encadrement**

`Taux d’encadrement`  = `BESOIN_HEURE`  / `HEURE_DISPONIBLE` %.

## Installation du projet 

* **clonez le dépôt :**  `git clone https://github.com/votreutilisateur/votreprojet.git` 

* Démarrez le serveur 
* ouvrer le fichier `credentials.php` dans `/Model` 
* Modifier les champs `login` et `password` afin de lier le site à votre base de données
* lancer le projet à travers votre serveur 
* Connecter vous au tant que l'un de ces [utilisateurs](#gestion-des-utilisateurs)

## L'équipe BASMOS  

* Hiba OUMSID
* Mathis VERCHERE
* Ilyes FATHI
* Adrian GALINDO
* Djibryl GUILLAUBY 

 [Les fonctionnalités du site](#les-fonctionnalités-de-notre-site)



## I

https://jupyter.univ-paris13.fr/~12206358/di-evoluzion-audit/ 

<?php
/**
 * @file Controller_consultation.php
 * @brief Ce controlleur gére les données  sur le module consultation 
*/
class Controller_consultation extends Controller {

/**
 * Affiche les heures de travail de l'utilisateur actuellement connecté.
 * 
 * @return void
 */
public function action_consultation(){
    $m = Model::getModel();
    
    $data["data"] = $m->getHeure($_SESSION["id"]);
    
    $data["profil"] = $m->getInfoProfil($_SESSION["id"]);
    
    $this->render("consultation_heure", $data);
}

/**
 * Affiche les informations globales liées à l'IUT.
 * 
 * @return void
 */

public function action_iut(){
    $m = Model::getModel();
    
    $data["data"] = $m->getIUT();

    $this->render("consultation_iut", $data);
}

/**
 * Action par défaut. Redirige vers l'action de consultation des heures de travail, si on accède directement au controlleur.
 * 
 * @return void
 */

public function action_default(){

    $this->action_consultation();
}
}
?>
<?php
/**
 * @file  Controller_connexion.php
 * @brief Ce controlleur gére les données de la page connexion
 */
class Controller_connexion extends Controller {

    /**
     * Vérifie si l'utilisateur est déjà connecté lorsqu'il essaye d'accéder à la page de connexion.
     * Empêche de charger le formulaire de connexion si une personne connectée essaye d'accéder à la page.
     * @return void
     */

    public function action_connexion(){
        // Vérifie que l'utilisateur est connecté ou pas.
        if(isset($_SESSION["connecte"]) and $_SESSION["connecte"]){
            header("Location: .");
            exit();
        }

        $this->render("connexion");
    }

    /**
     * action_connexion est appelé par défault lorsqu'on tente d'accèder à la page par le controlleur.
     * @return void
     */

    public function action_default(){
        $this->action_connexion();
    }

    /**
     * Vérifie les identifiants de l'utilisateur et gère la session de connexion.
     *
     * Cette méthode charge le modèle pour interagir avec la base de données,
     * appelle une fonction du modèle pour récupérer le mot de passe chiffré
     * associé à l'utilisateur, et vérifie si le mot de passe entré correspond.
     * Si les identifiants sont corrects, la session est mise à jour pour indiquer
     * que l'utilisateur est connecté. L'utilisateur est ensuite redirigé vers
     * la page d'accueil. S'il n'est pas connecté, la vue de connexion est affichée
     * avec un message d'erreur approprié.
     * 
     * @return void
     */

    public function action_check() {

        $m = Model::getModel();
        $infos = $m->identification_Check();

        // Vérifier si l'utilisateur existe et si le mot de passe est correct.
        if ($infos != false) {
            if (password_verify($_POST['mdp'], $infos["mdp"])) {
                // Mettre à jour la session pour indiquer que l'utilisateur est connecté.
                $_SESSION["connecte"] = true; 
                $_SESSION["id"] = $infos["id"];
                $_SESSION["permission"] = $infos["fonction"];

                // Si l'utilisateur a un identifiant spécifique, on ajuste les permissions.
                if ($infos["id"] == 123) {
                    $_SESSION["permission"] = "direction";
                }
            } else {
                // Mot de passe incorrect.
                $this->render("connexion", ['message' => 'Mot de passe ou login incorrect']);
                return;
            }
        } else {
            // Utilisateur inexistant.
            $this->render("connexion", ['message' => 'Cet utilisateur n\'existe pas']);
            return;
        }

        // Rediriger vers la page d'accueil.
        header("Location: .");
    }


    /**
     * Connexion à false afin de bloquer l'accès aux informations et de rediriger vers la page de connexion.
     * 
     * @return void
     */

    public function action_deconnexion(){
        $_SESSION["connecte"]=false;
        header("Location: .");
    }
}
?>
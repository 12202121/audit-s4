<?php

/**
 * @file Controller_demande.php
 * @brief Ce controlleur gére les données de #### à voir ### 
 */
class Controller_demande extends Controller {

    /**
     * Affiche la page de demande avec les données nécessaires.
     * 
     * Cette méthode charge le modèle pour récupérer les demandes
     * et les passe à la vue pour affichage.
     * 
     * @return void
     */
    public function action_demande() {

        $m = Model::getModel();

        $data['demande'] = $m->getDemande();

        $this->render("demande", $data);
    }

    /**
     * Action par défaut appelée lorsqu'on tente d'accéder à la page via le contrôleur.
     * Redirige vers la méthode action_demande().
     * 
     * @return void
     */
    public function action_default() {
        $this->action_demande();
    }
}

?>

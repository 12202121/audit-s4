<?php

/**
 * @file Controller_departement.php
 * @brief Ce controlleur gére les données du module département 
 */

class Controller_departement extends Controller {

    /**
     * Affiche la page du département avec les données appropriées.
     * 
     * Cette méthode vérifie les permissions de l'utilisateur avant
     * de charger les données du département et d'afficher la vue correspondante.
     * 
     * @return void
     */
    public function action_departement() {
        // Vérifie les permissions de l'utilisateur.
        if ($_SESSION["permission"] == "chefdedpt") {
            $m = Model::getModel();
            $data['info'] = $m->getInfoDepartement($_SESSION["id"]);
            $data['nomf'] = $m->getNomFormationPropose($data['info']['iddepartement']);
            $data['effectif'] = $m->getEffectifDpt($data['info']['iddepartement']);
            $data['besoinh'] = $m->getBesoinHeureDpt($data['info']['iddepartement']);
            $this->render("departement", $data);

        } elseif ($_SESSION["permission"] == "direction") {
            $m = Model::getModel();
            $data["libelledept"] = $m->getNomDepartement();
            if (isset($_GET["id"])) {
                $data["info"] = $m->getInfoDepartement2($_GET["id"]);
                $data['nomf'] = $m->getNomFormationPropose($data['info']['iddepartement']);
                $data['effectif'] = $m->getEffectifDpt($data['info']['iddepartement']);
                $data['besoinh'] = $m->getBesoinHeureDpt($data['info']['iddepartement']);
                $this->render("departement", $data);
            } else {
                $this->render("list_dpt", $data);
            }
        } else {

            $this->action_error("Vous n'avez pas les permissions");
        }
    }

    /**
     * Affiche le formulaire de demande avec les données nécessaires.
     * 
     * Cette méthode vérifie les permissions de l'utilisateur avant
     * de charger les données nécessaires pour le formulaire de demande
     * et d'afficher la vue correspondante.
     * 
     * @return void
     */
    public function action_demande() {
        // Vérifie les permissions de l'utilisateur.
        if ($_SESSION["permission"] == "chefdedpt" || $_SESSION["permission"] == "direction") {
            $m = Model::getModel();
            $data = [
                "annee" => $m->getAnnee(),
                "semestre" => $m->getSemestre(),
                "departement" => $m->getDpt(),
                "discipline" => $m->getDiscipline(),
                "formation" => $m->getFormation()
            ];
            $this->render("demande_form", $data);
        } else {
            $this->action_default();
        }
    }

    /**
     * Valide et enregistre les besoins du département.
     * 
     * Cette méthode vérifie les données de l'utilisateur avant
     * de les enregistrer dans la base de données et d'afficher un message de succès
     * ou d'erreur en cas de données non valides.
     * 
     * @return void
     */
    public function action_validation() {
        $m = Model::getModel();
        // Vérifie si le champ "besoin" contient uniquement des chiffres.
        if (preg_match("/^[0-9]+$/", $_POST["besoin"])) {
            $m->ajouterBesoin();
            if ($_SESSION["permission"] == "chefdedpt") {
                $m = Model::getModel();
                $data['info'] = $m->getInfoDepartement($_SESSION["id"]);
                $data['nomf'] = $m->getNomFormationPropose($data['info']['iddepartement']);
                $data['effectif'] = $m->getEffectifDpt($data['info']['iddepartement']);
                $data['besoinh'] = $m->getBesoinHeureDpt($data['info']['iddepartement']);
                $this->render("departement", $data);
    
            } elseif ($_SESSION["permission"] == "direction") {
                $m = Model::getModel();
                $data["libelledept"] = $m->getNomDepartement();
                if (isset($_GET["id"])) {
                    $data["info"] = $m->getInfoDepartement2($_GET["id"]);
                    $data['nomf'] = $m->getNomFormationPropose($data['info']['iddepartement']);
                    $data['effectif'] = $m->getEffectifDpt($data['info']['iddepartement']);
                    $data['besoinh'] = $m->getBesoinHeureDpt($data['info']['iddepartement']);
                    $this->render("departement", $data);
                } else {
                    $this->render("list_dpt", $data);
                }
            } else {
    
                $this->action_error("Vous n'avez pas les permissions");
            }
        }
        else {
            $this->action_error("Informations non valide !");
        }
    }

    /**
     * Action par défaut appelée lorsqu'on tente d'accéder à la page via le contrôleur.
     * Redirige vers la méthode action_departement().
     * 
     * @return void
     */
    public function action_default() {
        $this->action_departement();
    }

}
?>

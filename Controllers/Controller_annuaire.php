<?php 
/**
 * @file Controller_annuaire.php 
 * @brief Ce controlleur gére les données du module Annuaire 
*/ 
class Controller_annuaire extends Controller {

    /**
     * Affiche la liste des utilisateurs dans l'annuaire.
     * Cette méthode charge le modèle pour récupérer la liste des utilisateurs et les passe à la vue pour affichage.
     * @return void
     */
    public function action_annuaire(){

        $m = Model::getModel();

        $data = ["infos" => $m->getList()];

        $this->render("annuaire", $data);
    }

    /**
     * Action par défaut appelée lorsqu'on tente d'accéder à la page via le contrôleur.
     * Redirige vers la méthode action_annuaire().
     * 
     * @return void
     */
    public function action_default(){
        $this->action_annuaire();
    }

    /**
     * Affiche le formulaire pour ajouter un nouvel utilisateur.
     * 
     * @return void
     */
    public function action_ajouter(){
        $this->render("ajouter");
    }

    /**
     * Affiche le formulaire avec les données nécessaires pour ajouter un nouvel utilisateur.
     * 
     * Cette méthode charge le modèle pour récupérer les catégories, les disciplines,
     * les départements, les années et les semestres, puis les passe à la vue pour affichage.
     * 
     * @return void
     */
    public function action_ajouter_form(){

        $m = Model::getModel();

        $data = [
            "list" => $m->getCatDiscDpt(),
            "annee" => $m->getAnnee(),
            "semestre" => $m->getSemestre()
        ];

        $this->render("form_ajouter", $data);
    }

    /**
     * Valide et ajoute un nouvel utilisateur à l'annuaire.
     * 
     * Cette méthode vérifie les informations soumises, puis ajoute l'utilisateur à la base de données
     * si les informations sont valides. Sinon, affiche un message d'erreur.
     * 
     * @return void
     */
    public function action_validation(){

        $m = Model::getModel();

        // Vérifier la validité de l'ID et s'il n'est pas déjà présent dans la base de données.
        if (preg_match("/^[0-9]+$/", $_POST["id"])) {
            if ($m->id_in_db($_POST["id"])) {
                $this->action_error("l'id existe déja dans la base de données !");
            }
            else {
                if ($m->email_in_db($_POST["email"])) {
                    $this->action_error("l'email existe déja dans la base de données !");
                }
                else {
                    if ($m->phone_in_db($_POST["phone"])) {
                        $this->action_error("le numéro de téléphone existe déja dans la base de données !");
                    }
                    else {
                        // Initialiser les informations de l'utilisateur.
                        $infos = [
                            "poste" => $_POST["poste"],
                            "id" => $_POST["id"],
                            "fonction" => $_POST["poste"],
                            "nom" => !preg_match("/^ *$/", $_POST["nom"]) ? $_POST["nom"] : "???",
                            "prenom" => !preg_match("/^ *$/", $_POST["prenom"]) ? $_POST["prenom"] : "???",
                            "email" => !preg_match("/^ *$/", $_POST["email"]) ? $_POST["email"] : "???",
                            "phone" => !preg_match("/^ *$/", $_POST["phone"]) ? $_POST["phone"] : "???",
                            "mdp" => !preg_match("/^ *$/", $_POST["mdp"]) ? password_hash($_POST["mdp"], PASSWORD_DEFAULT) : "0"
                        ];

                        // Si le poste est enseignant, ajouter les informations supplémentaires.
                        if ($_POST["poste"] == "enseignant") {
                            $infos = array_merge($infos, [
                                "annee" => $_POST["annee"],
                                "semestre" => $_POST["semestre"],
                                "statut" => $_POST["statut"],
                                "discipline" => $_POST["discipline"],
                                "direction" => $_POST["direction"]
                            ]);
                            if (isset($_POST["departements"])) {
                                $infos["departements"] = $_POST["departements"];
                            }
                        }


                        $m->ajouterUtilisateur($infos);
                        $data = ["infos" => $m->getList()];

                        $this->render("annuaire", $data);
                    }
                }
            }
            
        } else {

            $this->action_error("Informations non valide !");
        }
    }

    /**
     * Supprime un utilisateur de l'annuaire.
     * 
     * Cette méthode vérifie les permissions de l'utilisateur connecté,
     * puis supprime l'utilisateur spécifié de la base de données s'il a les droits appropriés.
     * 
     * @return void
     */
    public function action_supprimer(){
        // Vérifier les permissions de l'utilisateur connecté.
        if (isset($_SESSION["permission"]) && $_SESSION["permission"] == "direction") {

            $m = Model::getModel();
            if ($_SESSION['id'] != $_GET["id"]) {
                $m = Model::getModel();

                $m->supprimerUtilisateur($_GET["id"]);
    
                $this->action_default();
            }
            else {

                $this->action_error("Vous ne pouvez pas vous supprimer vous même !");

            }

        }
    }

    /** 
     * Vérifie si un ID est déjà présent dans la base de données.
     * 
     * @return void
     */
    public function action_check_id() {
        $m = Model::getModel();
        $id = $_POST["id"];
        $exists = $m->id_in_db($id);
        echo json_encode(["exists" => $exists]);
    }

    /** 
     * Vérifie si un email est déjà présent dans la base de données.
     * 
     * @return void
     */
    public function action_check_email() {
        $m = Model::getModel();
        $email = $_POST["email"];
        $exists = $m->email_in_db($email);
        echo json_encode(["exists" => $exists]);
    }

    /** 
     * Vérifie si un numéro de télphone est déjà présent dans la base de données.
     * 
     * @return void
     */
    public function action_check_phone() {
        $m = Model::getModel();
        $phone = $_POST["phone"];
        $exists = $m->phone_in_db($phone);
        echo json_encode(["exists" => $exists]);
    }

}
?>

<?php

/**
 * @file ControlleurAcceuil.php
 * @brief ce controlleur affiche la page d'accueil avec les informations de profil de l'utilisateur s'il est connecté.
 * @version 1.0
*/ 

class Controller_accueil extends Controller {

    /** 
     * Cette méthode charge le modèle pour interagir avec la base de données,
     * récupère les informations du profil de l'utilisateur s'il est connecté,
     * et les passe à la vue pour affichage.
     * 
     * @return void
     */
    public function action_accueil() {
        /**
         * @var $m contient le model et lien connexion 
         */
        $m = Model::getModel();
        /**
         * @var $data tableau qui contiendra les données renvyé vers la vue  
         */
        $data = [];

        // Si l'utilisateur est connecté, récupérer ses informations de profil.
        if (isset($_SESSION["id"])) {
            $data = $m->getInfoProfil($_SESSION["id"]);
        }

        $this->render("accueil", $data);
    }

    /**
     * Action par défaut appelée lorsqu'on tente d'accéder à la page via le contrôleur.
     * Redirige vers la méthode action_accueil().
     * @brief Affiche la page d'accueil avec les informations de profil de l'utilisateur s'il est connecté.
     * @param a fait hdbdhvf
     * @version 1.0
     * @details Cette méthode charge le modèle pour interagir avec la base de données,
     * @return void
     */
    public function action_default() {
        $this->action_accueil();
    }
}
?>

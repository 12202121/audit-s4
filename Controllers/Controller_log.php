<?php


/**
 * @file Controller_log.php
 * @brief Ce controlleur gérer les données lié à la page de connexion 
 */

class Controller_log extends Controller {

    /**
     * Affiche les journaux de log.
     * 
     * Cette méthode charge les journaux de log depuis le modèle et
     * les affiche dans la vue correspondante.
     * 
     * @return void
     */
    public function action_log() {   

        $m = Model::getModel();

        $data['log'] = $m->getLog();

        $this->render("log", $data);
    }

    /**
     * Action par défaut appelée lorsqu'on tente d'accéder à la page via le contrôleur.
     * Redirige vers la méthode action_log().
     * 
     * @return void
     */
    public function action_default() {
        $this->action_log();
    }
}
?>

<?php
class Controller_profil extends Controller {

    /**
     * Affiche le profil de l'utilisateur.
     * 
     * Cette méthode récupère les informations du profil à afficher.
     * Si un identifiant est passé en paramètre et qu'il est valide, 
     * les informations de ce profil sont récupérées. Sinon, les informations 
     * du profil de l'utilisateur connecté sont affichées.
     * 
     * @return void
     */
    public function action_profil() {
        $m = Model::getModel();
        
        // Vérifie si un identifiant est passé en paramètre et s'il est valide.
        if (isset($_GET["id"]) && $m->id_in_db($_GET["id"])) {

            $data = $m->getInfoProfil($_GET["id"]);
        } else {

            $data = $m->getInfoProfil($_SESSION["id"]);
        }
        

        $this->render("profil", $data);
    }

    /**
     * Action par défaut appelée lorsqu'on tente d'accéder à la page via le contrôleur.
     * Redirige vers la méthode action_profil().
     * 
     * @return void
     */
    public function action_default() {
        $this->action_profil();
    } 

    /**
     * Affiche la page de modification de profil.
     * 
     * Cette méthode charge les informations actuelles du profil à modifier et les 
     * données nécessaires pour la modification (catégories, disciplines, années, semestres).
     * 
     * @return void
     */
    public function action_modification() {
        $m = Model::getModel();
        $id = $_SESSION["id"];
        
        // Vérifie si un identifiant est passé en paramètre et si l'utilisateur a les permissions nécessaires.
        if (isset($_GET["id"]) && $_SESSION["permission"] == "direction") {
            $id = $_GET["id"];
        }
        
        $data = [
            'profil' => $m->getInfoProfil($id),
            'list' => $m->getCatDiscDpt(),
            'annee' => $m->getAnnee(),
            'semestre' => $m->getSemestre()
        ];
        
        $this->render("modification", $data);
    }

    /**
     * Modifie les informations du profil.
     * 
     * Cette méthode récupère les informations du formulaire de modification de profil,
     * met à jour le profil de l'utilisateur dans la base de données, et redirige vers
     * la page de profil mise à jour.
     * 
     * @return void
     */
    public function action_modifier() {
        // Vérifie si un identifiant est passé dans le formulaire.
        if (isset($_POST["id"])) {

            $infos["id"] = $_POST["id"];
            $infos["nom"] = $_POST["nom"];
            $infos["prenom"] = $_POST["prenom"];

            $email = trim($_POST['email']);

            if (!empty($email) && preg_match('/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/', $email)) {
                $infos["email"] = e($email);
            }
            else {
                $this->render("message", ["title" => "Information", "message" => "Email non conforme !"]);
            }

            $phone = trim($_POST['phone']);

            if (!empty($phone) && preg_match('/^[0-9]{10}$/', $phone)){
                $infos["phone"] = e($phone);
            }
            else {
                $this->render("message", ["title" => "Information", "message" => "Numéro de téléphone non conforme !"]);
            }
            
            
            // Vérifie si le champ mot de passe est vide.
            if (preg_match("/^ *$/", $_POST["mdp"])) {

                $this->render("message", ["title" => "Information", "message" => "Mot de passe manquant !"]);
            } else {

                $infos["mdp"] = password_hash($_POST["mdp"], PASSWORD_DEFAULT);
            }
            
            // Vérifie si la fonction n'est pas "secretaire" ou "personne".
            if ($_POST["fonction"] != "secretaire" && $_POST["fonction"] != "personne" && isset($_POST["departements"])) {

                $infos['annee'] = $_POST["annee"];
                $infos['semestre'] = $_POST["semestre"];
                $infos["statut"] = $_POST["statut"];
                $infos["discipline"] = $_POST["discipline"];
                $infos["departements"] = $_POST["departements"];
            }
            
            

            $infos["fonction"] = $_POST["fonction"];
            

            $m = Model::getModel();
            $m->updateProfil($infos);
            

            $this->action_default();
        } else {

            $this->render("message", ["title" => ":)", "message" => "Modification non réussie !"]);
        }
    }
}
?>

<?php 

/**
 * @file Controller_declaration.php
 * @brief Ce controlleur gére les données du module déclaration 
 */
class Controller_declaration extends Controller {

    /**
     * Affiche le formulaire de déclaration avec les données nécessaires.
     * 
     * Cette méthode charge le modèle pour récupérer les années, semestres et départements,
     * et les passe à la vue pour affichage.
     * 
     * @return void
     */
    public function action_declaration(){   

        $m = Model::getModel();

        $data = [
            "annee" => $m->getAnnee(),
            "semestre" => $m->getSemestre(),
            "departement" => $m->getDpt()
        ];

        $this->render("declaration_form", $data);
    }

    /**
     * Action par défaut appelée lorsqu'on tente d'accéder à la page via le contrôleur.
     * Redirige vers la méthode action_declaration().
     * 
     * @return void
     */
    public function action_default(){
        $this->action_declaration();
    }

    /**
     * Valide et traite les données soumises par le formulaire de déclaration.
     * 
     * Cette méthode vérifie que le nombre d'heures est valide, ajoute l'heure déclarée
     * via le modèle, et affiche un message de succès. En cas d'erreur, elle affiche
     * un message d'erreur.
     * 
     * @return void
     */
    public function action_validation(){

        $m = Model::getModel();
        // Vérifier que le nombre d'heures est un nombre valide.
        if(preg_match("/^[0-9]+$/", $_POST["heure"])){
            if (isset($_SESSION["id"])) {
                $data = $m->getInfoProfil($_SESSION["id"]);
                $m->ajouterHeure();

                $this->render("accueil", $data);
            
            }
        } else {

            $this->action_error("Informations non valides !");
        }
    }
}

?>

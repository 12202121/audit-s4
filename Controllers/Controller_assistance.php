<?php 
/**
 * @file Controller_assistance.php
 * @brief Ce controlleur gére les données du module assistance  
 */
class Controller_assistance extends Controller {

    /**
     * 
     * Affiche la page d'assistance avec les informations de profil de l'utilisateur connecté.
     * 
     * Cette méthode charge le modèle pour récupérer les informations de profil de l'utilisateur
     * connecté et les passe à la vue pour affichage.
     * 
     * @return void
     */
    public function action_assistance(){   

        $m = Model::getModel();

        $data = ["profil" => $m->getInfoProfil($_SESSION['id'])];

        $this->render("assistance", $data);
    }

    /**
     * Action par défaut appelée lorsqu'on tente d'accéder à la page via le contrôleur.
     * Redirige vers la méthode action_assistance().
     * 
     * @return void
     */
    public function action_default(){
        $this->action_assistance();
    }

    /**
     * Action pour envoyer un email avec les paramètres entrés en POST.
     * 
     * Cette méthode utilise la classe EmailSender pour envoyer un email.
     * 
     * @return void
     */
    public function action_send_email(){
        if(isset($_POST['subject'], $_POST['message'])){
            $email = "basmos.team@gmail.com";
            $subject = $_POST['subject'];
            $message = $_POST['message'];

            EmailSender::sendVerificationEmail($email, e($subject) . e($_POST['id']) . e($_POST['email']), e($message));

            $data = ['message' => 'Email envoyé avec succès à ' . htmlspecialchars($email)];
            $this->action_assistance();
        } else {
            $this->action_assistance();
        }
    }
}


?>
